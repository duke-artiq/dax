{ pkgs ? import <nixpkgs> { }
, artiqpkgs ? import <artiq-full> { inherit pkgs; }
}:

let
  dax = import ./default.nix { inherit pkgs artiqpkgs; };
in
(pkgs.python3.withPackages (ps: [
  dax
])).env
