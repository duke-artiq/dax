import typing
import unittest.mock

import artiq.coredevice.edge_counter

from dax.experiment import *
from dax.interfaces.detection import DetectionInterface
from dax.modules.cool_context import CoolContextCounter

import test.hw_test


class _MockDetectionModule(DaxModule, DetectionInterface):

    def build(self, state_detection_threshold: int, num_channels: int):  # type: ignore[override]
        self.state_detection_threshold = state_detection_threshold
        self.num_channels = num_channels

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass

    def get_pmt_array(self) -> typing.List[artiq.coredevice.edge_counter.EdgeCounter]:
        return [unittest.mock.Mock(artiq.coredevice.edge_counter.EdgeCounter) for _ in range(self.num_channels)]

    def get_state_detection_threshold(self) -> int:
        return self.state_detection_threshold

    def get_default_detection_time(self) -> float:
        return 100 * us


class _CoolContextCounterTestSystem(DaxSystem):
    SYS_ID = 'unittest_system'
    SYS_VER = 0
    CORE_LOG_KEY = None
    DAX_INFLUX_DB_KEY = None

    NUM_CHANNELS = 5

    def build(self, state_detection_threshold=2, auto_reset=True, **kwargs) -> None:  # type: ignore[override]
        super().build()
        self.detection = _MockDetectionModule(self, 'detection',
                                              state_detection_threshold=state_detection_threshold,
                                              num_channels=self.NUM_CHANNELS)
        self.cool_context = CoolContextCounter(self, 'cool_context_counter', auto_reset=auto_reset, **kwargs)


class CoolContextCounterTestCase(test.hw_test.HardwareTestCase):
    SYSTEM_TYPE = _CoolContextCounterTestSystem

    def test_reset_counters(self):
        class Env(self.SYSTEM_TYPE, Experiment):
            TEST_DATA = [
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 1, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 9],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 2, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 8],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 2, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 7],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 6],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 5],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 4],
            ]

            @kernel
            def run(self):
                self.core.reset()
                # Append data
                with self.cool_context:
                    for d in self.TEST_DATA:
                        self.cool_context.append(d)
                # Reset counters
                self.cool_context.reset_counters()
                # Test
                for c in self.cool_context._counter:
                    if c != 0:
                        return False  # Test failure, a counter was not reset correctly
                return True  # Test success

        # Run tests
        env = self.construct_env(Env)
        env.dax_init()
        self.assertTrue(env.run())
        self.assertListEqual(env.cool_context._counter, [0] * env.NUM_CHANNELS)

    def test_get_num_lost_ions(self):
        class Env(self.SYSTEM_TYPE, Experiment):
            TEST_DATA = [
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 1, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 9],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 2, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 8],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 2, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 7],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 6],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 5],
                [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 4],
            ]

            @kernel
            def run(self):
                self.core.reset()
                # Append data
                with self.cool_context:
                    for d in self.TEST_DATA:
                        self.cool_context.append(d)
                # Test
                num_violations = len(self.TEST_DATA)
                a = self.cool_context.get_num_lost_ions(num_violations - 1)
                b = self.cool_context.get_num_lost_ions(num_violations)
                c = self.cool_context.get_num_lost_ions(num_violations + 1)
                # Return results
                return a, b, c

        # Run tests
        env = self.construct_env(Env)
        env.dax_init()
        a, b, c = env.run()
        self.assertEqual(a, 1)
        self.assertEqual(b, 1)
        self.assertEqual(c, 0)
        counters_ref = [0] * env.NUM_CHANNELS
        counters_ref[1] = len(env.TEST_DATA)
        self.assertListEqual(env.cool_context._counter, counters_ref)
