import typing
import contextlib

from artiq.experiment import *
from dax.scan import DaxScanIndexType, DaxScan

import test.hw_test


class _ScanExperiment(DaxScan, HasEnvironment):
    ENABLE_SCAN_INDEX: typing.ClassVar[typing.Union[bool, DaxScanIndexType]] = False

    def build_scan(self) -> None:
        self.setattr_device('core')

        self.add_scan('foo', 'foo', Scannable(ExplicitScan([1, 1])))
        self.add_iterator('foobar', 'foobar', 2)
        self.add_static_scan('bar', [1, 1])
        self.result = 0
        self.index = 0
        self.host_setup_done = False
        self.device_setup_done = False
        self.device_cleanup_done = False
        self.host_cleanup_done = False

    def host_setup(self):
        self.host_setup_done = True

    @kernel
    def device_setup(self):
        self.device_setup_done = True

    @kernel
    def run_point(self, point, index):
        self.result += point.foo
        self.result += point.bar

    @kernel
    def device_cleanup(self):
        self.device_cleanup_done = True

    def host_cleanup(self):
        self.host_cleanup_done = True

    def get_identifier(self):
        return self.__class__.__name__


class ScanKernelTestCase(test.hw_test.HardwareTestCase):
    SCAN_EXPERIMENT_CLS = _ScanExperiment

    def test_run(self, env_cls=None, result=16, *, host=True, device=True, exception=False):
        env = self.construct_env(self.SCAN_EXPERIMENT_CLS if env_cls is None else env_cls)
        with self.assertRaises(RuntimeError) if exception else contextlib.nullcontext():
            env.run()
        self.assertEqual(env.result, result)
        self.assertEqual(env.host_setup_done, host)
        self.assertEqual(env.device_setup_done, device)
        self.assertEqual(env.device_cleanup_done, device)
        self.assertEqual(env.host_cleanup_done, host)

    def test_run_exception(self):
        class Env(self.SCAN_EXPERIMENT_CLS):
            @kernel
            def run_point(self, point, index):
                raise RuntimeError

        # Attributes only sync if there is no exception in the kernel
        self.test_run(Env, 0, device=False, exception=True)

    def test_host_setup_exception(self):
        class Env(self.SCAN_EXPERIMENT_CLS):
            def host_setup(self):
                super(Env, self).host_setup()
                raise RuntimeError

        self.test_run(Env, 0, device=False, exception=True)

    def test_device_setup_exception(self):
        class Env(self.SCAN_EXPERIMENT_CLS):
            @kernel
            def device_setup(self):
                raise RuntimeError

        # Attributes only sync if there is no exception in the kernel
        self.test_run(Env, 0, device=False, exception=True)

    def test_device_cleanup_exception(self):
        class Env(self.SCAN_EXPERIMENT_CLS):
            @kernel
            def device_cleanup(self):
                self.device_cleanup_done = True
                raise RuntimeError

        # Attributes only sync if there is no exception in the kernel
        self.test_run(Env, 0, device=False, exception=True)

    def test_host_cleanup_exception(self):
        class Env(self.SCAN_EXPERIMENT_CLS):
            def host_cleanup(self):
                super(Env, self).host_cleanup()
                raise RuntimeError

        self.test_run(Env, device=True, exception=True)


class _ScanExperimentIndexTypeDisable(_ScanExperiment):
    ENABLE_SCAN_INDEX = DaxScanIndexType.DISABLE


class ScanKernelIndexTypeDisableTestCase(ScanKernelTestCase):
    SCAN_EXPERIMENT_CLS = _ScanExperimentIndexTypeDisable


class _ScanExperimentIndexTypeEnable(_ScanExperiment):
    ENABLE_SCAN_INDEX = DaxScanIndexType.ENABLE

    @kernel
    def run_point(self, point, index):
        self.result += point.foo
        self.result += point.bar
        self.index += index.foo + index.foobar + index.bar  # Just to see if this compiles


class ScanKernelIndexTypeEnableTestCase(ScanKernelTestCase):
    SCAN_EXPERIMENT_CLS = _ScanExperimentIndexTypeEnable


class _ScanExperimentIndexTypeFlag(_ScanExperiment):
    ENABLE_SCAN_INDEX = DaxScanIndexType.FLAG

    @kernel
    def run_point(self, point, index):
        self.result += point.foo
        self.result += point.bar
        assert index.foo is True or index.foo is False  # Just to see if this compiles
        assert index.foobar is True or index.foobar is False  # Just to see if this compiles
        assert index.bar is True  # Just to see if this compiles


class ScanKernelIndexTypeFlagTestCase(ScanKernelTestCase):
    SCAN_EXPERIMENT_CLS = _ScanExperimentIndexTypeFlag


class _ScanExperimentIndexTypeIndexFlag(_ScanExperiment):
    ENABLE_SCAN_INDEX = DaxScanIndexType.INDEX_FLAG

    @kernel
    def run_point(self, point, index):
        self.result += point.foo
        self.result += point.bar
        foo_i, foo_f = index.foo
        foobar_i, foobar_f = index.foobar
        bar_i, bar_f = index.bar
        self.index += foo_i + foobar_i + bar_i  # Just to see if this compiles
        assert foo_f is True or foo_f is False  # Just to see if this compiles
        assert foobar_f is True or foobar_f is False  # Just to see if this compiles
        assert bar_f is True  # Just to see if this compiles


class ScanKernelIndexTypeIndexFlagTestCase(ScanKernelTestCase):
    SCAN_EXPERIMENT_CLS = _ScanExperimentIndexTypeIndexFlag
