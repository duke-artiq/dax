import sys
import unittest

import dax.sim
import dax.sim.coredevice.controller
import dax.util.artiq
import dax.util.controller

from test.dummy_driver import DummyDriver
from test.environment import CI_ENABLED
from test.network import find_free_port


class _TestDriver:

    def __init__(self):
        self._open = self._close = self._enter = self._exit = 0

    def open(self):
        self._open += 1

    def close(self):
        self._close += 1

    def __enter__(self):
        self._enter += 1

    def __exit__(self, *_):
        self._exit += 1


class ControllerTestCase(unittest.TestCase):

    def test_lca(self):
        lca = dax.util.controller.LocalControllerAdapter(None, __name__, _TestDriver.__name__, {}, is_context=True)
        try:
            self.assertEqual(lca._controller._open, 0)
            self.assertEqual(lca._controller._close, 0)
            self.assertEqual(lca._controller._enter, 1)  # is_context=True calls __enter__()
            self.assertEqual(lca._controller._exit, 0)

            lca.open()
            self.assertEqual(lca._controller._open, 1)  # open() call
            self.assertEqual(lca._controller._close, 0)
            self.assertEqual(lca._controller._enter, 1)
            self.assertEqual(lca._controller._exit, 0)

            lca.close()
            self.assertEqual(lca._controller._open, 1)
            self.assertEqual(lca._controller._close, 0)  # close() closes the adapter, which calls __exit__()
            self.assertEqual(lca._controller._enter, 1)
            self.assertEqual(lca._controller._exit, 1)  # close() closes the adapter, which calls __exit__()
        finally:
            lca.close()

    def test_lca_ddb(self):
        ddb = dax.sim.enable_dax_sim({
            'core': {
                'type': 'local',
                'module': 'artiq.coredevice.core',
                'class': 'Core',
                'arguments': {'host': None, 'ref_period': 1e-9}
            },
            'core_cache': {
                'type': 'local',
                'module': 'artiq.coredevice.cache',
                'class': 'CoreCache'
            },
            'core_dma': {
                'type': 'local',
                'module': 'artiq.coredevice.dma',
                'class': 'CoreDMA'
            },
            "lca": {
                "type": "local",
                "module": "dax.util.controller",
                "class": "LocalControllerAdapter",
                "arguments": {
                    "controller_module": DummyDriver.__module__,
                    "controller_class": DummyDriver.__name__,
                    "arguments": {},
                    "is_context": False,
                }
            },
        }, enable=True, output="null", moninj_service=False)
        managers = dax.util.artiq.get_managers(ddb)

        # Unpack managers
        device_mgr, _, _, _ = managers
        # Get ddb
        ddb = device_mgr.get_device_db()
        assert ddb
        # Test device
        device = device_mgr.get("lca")
        assert isinstance(device, dax.sim.coredevice.controller.LocalControllerAdapter)
        assert device.ping()
        assert device.is_simulation()

    @unittest.skipUnless(CI_ENABLED, 'Not in a CI environment, skipping slow test')
    def test_sca(self):
        sca = dax.util.controller.SubprocessControllerAdapter(
            None,
            find_free_port(),
            f"{sys.executable} -m {DummyDriver.__module__} -p {{port}} --bind {{bind}} --no-localhost-bind",
            host="127.0.0.1",  # Run on IPv4 for testing in docker
        )
        try:
            assert sca.ping()
        finally:
            sca.close()

    @unittest.skipUnless(CI_ENABLED, 'Not in a CI environment, skipping slow test')
    def test_sca_ddb(self):
        ddb = dax.sim.enable_dax_sim({
            'core': {
                'type': 'local',
                'module': 'artiq.coredevice.core',
                'class': 'Core',
                'arguments': {'host': None, 'ref_period': 1e-9}
            },
            'core_cache': {
                'type': 'local',
                'module': 'artiq.coredevice.cache',
                'class': 'CoreCache'
            },
            'core_dma': {
                'type': 'local',
                'module': 'artiq.coredevice.dma',
                'class': 'CoreDMA'
            },
            "sca": {
                "type": "local",
                "module": "dax.util.controller",
                "class": "SubprocessControllerAdapter",
                "arguments": {
                    "host": "127.0.0.1",  # Run on IPv4 for testing in docker
                    "port": find_free_port(),
                    "command": f"{sys.executable} -m {DummyDriver.__module__} -p {{port}} --bind {{bind}} "
                               "--no-localhost-bind",
                }
            },
        }, enable=True, output="null", moninj_service=False)
        managers = dax.util.artiq.get_managers(ddb)

        # Unpack managers
        device_mgr, _, _, _ = managers
        # Get ddb
        ddb = device_mgr.get_device_db()
        assert ddb
        # Test device
        device = device_mgr.get("sca")
        assert isinstance(device, dax.sim.coredevice.controller.SubprocessControllerAdapter)
        assert device.ping()
        assert device.is_simulation()
