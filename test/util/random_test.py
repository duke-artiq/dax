import numpy as np
import random
import unittest

import dax.util.random

from test.environment import CI_ENABLED

_NUM_ITERATIONS = 10000 if CI_ENABLED else 100

_rng = random.Random()


def _random_seed():
    """An int64 seed that is valid for the test class only."""
    return _rng.randrange(-2**63, 2**63)


def _random_useed():
    """A seed that is valid for the test class and the reference class."""
    return _rng.randrange(2**63)


def _suppress_warnings():
    sup = np.testing.suppress_warnings()
    sup.filter(RuntimeWarning, "overflow encountered in .*")
    return sup


class _PCG_XSH_RR:
    """PCG-XSH-RR-64-32 reference implementation."""

    DEFAULT_INCREMENT = np.uint64(1442695040888963407)
    _MULTIPLIER = np.uint64(6364136223846793005)

    _state: np.uint64
    _increment: np.uint64

    def __init__(self, *, increment=DEFAULT_INCREMENT):
        self._state = np.uint64(0)
        self._increment = np.uint64(increment | np.uint64(1))

    def _step_r(self):
        self._state = np.uint64(np.uint64(self._state * self._MULTIPLIER) + self._increment)

    def init(self, seed):
        self._state = np.uint64(0)
        self._step_r()
        self._state = np.uint64(self._state + np.uint64(seed))
        self._step_r()

    def _randuint32(self):
        # Store old state
        oldstate = self._state
        # Advance state
        self._step_r()
        # Apply output function on old state
        xorshifted = np.uint32(((oldstate >> np.uint64(18)) ^ oldstate) >> np.uint64(27))
        rot = np.uint32(oldstate >> np.uint64(59))
        return np.uint32((xorshifted >> rot) | np.uint32(xorshifted << np.uint32((-rot) & 31)))

    def randint32(self):
        return np.int32(self._randuint32())

    def _randuint64(self):
        lower = np.uint64(self._randuint32())
        upper = np.uint64(self._randuint32())
        return np.uint64(lower | np.uint64(upper << np.uint64(32)))

    def randint64(self):
        return np.int64(self._randuint64())

    def randrange(self, start, stop):
        # Based on OpenBSD's method, see https://www.pcg-random.org/posts/bounded-rands.html
        span = np.uint32(max(1, stop - start))
        threshold = np.uint32(-span % span)
        while True:
            r = self._randuint32()
            if r >= threshold:
                return np.int32(np.uint32(r % span) + start)


class PCG_XSH_RRTestCase(unittest.TestCase):

    def test_init(self):
        rng = dax.util.random.PCG_XSH_RR(None)
        self.assertIsInstance(rng._MULTIPLIER, np.int64)
        self.assertIsInstance(rng._state, np.int64)
        self.assertEqual(rng._state, 0)
        self.assertIsInstance(rng._increment, np.int64)
        self.assertEqual(rng._increment, rng.DEFAULT_INCREMENT)

    def test_init_increment(self):
        for increment in range(10):
            rng = dax.util.random.PCG_XSH_RR(None, increment=increment)
            self.assertIsInstance(rng._increment, np.int64)
            self.assertEqual(rng._increment, increment if increment % 2 else increment + 1)

    def test_randint32_ref(self):
        # Create a seed, a prng, and a reference prng
        seed = _random_useed()
        rng = dax.util.random.PCG_XSH_RR(None)
        ref = _PCG_XSH_RR()

        with _suppress_warnings():
            # Create a sequence
            rng.init(seed)
            first = [rng.randint32() for _ in range(_NUM_ITERATIONS)]
            # Create a reference sequence
            ref.init(seed)
            second = [ref.randint32() for _ in range(_NUM_ITERATIONS)]
            # Compare sequences
            self.assertListEqual(first, second)

    def test_randint64_ref(self):
        # Create a seed, a prng, and a reference prng
        seed = _random_useed()
        rng = dax.util.random.PCG_XSH_RR(None)
        ref = _PCG_XSH_RR()

        with _suppress_warnings():
            # Create a sequence
            rng.init(seed)
            first = [rng.randint64() for _ in range(_NUM_ITERATIONS)]
            # Create a reference sequence
            ref.init(seed)
            second = [ref.randint64() for _ in range(_NUM_ITERATIONS)]
            # Compare sequences
            self.assertListEqual(first, second)

    def test_randrange_ref(self):
        # Create a seed, a prng, and a reference prng
        seed = _random_useed()
        rng = dax.util.random.PCG_XSH_RR(None)
        ref = _PCG_XSH_RR()

        with _suppress_warnings():
            for start, stop in [(0, 100), (0, 1), (-20, 20), (-1000, -1), (-2**31, 2**31 - 1)]:
                # Create a sequence
                rng.init(seed)
                first = [rng.randrange(start, stop) for _ in range(_NUM_ITERATIONS)]
                # Create a reference sequence
                ref.init(seed)
                second = [ref.randrange(start, stop) for _ in range(_NUM_ITERATIONS)]
                # Compare sequences
                self.assertListEqual(first, second)

    def test_randint32(self):
        # Create a seed and a prng
        seed = _random_seed()
        rng = dax.util.random.PCG_XSH_RR(None)

        with _suppress_warnings():
            # Create a first sequence
            rng.init(seed)
            first = [rng.randint32() for _ in range(_NUM_ITERATIONS)]
            # Check first sequence
            self.assertTrue(all(isinstance(r, np.int32) for r in first))
            # Create a second sequence
            rng.init(seed)
            second = [rng.randint32() for _ in range(_NUM_ITERATIONS)]
            # Compare sequences
            self.assertListEqual(first, second)

    def test_randint64(self):
        # Create a seed and a prng
        seed = _random_seed()
        rng = dax.util.random.PCG_XSH_RR(None)

        with _suppress_warnings():
            # Create a first sequence
            rng.init(seed)
            first = [rng.randint64() for _ in range(_NUM_ITERATIONS)]
            # Check first sequence
            self.assertTrue(all(isinstance(r, np.int64) for r in first))
            # Create a second sequence
            rng.init(seed)
            second = [rng.randint64() for _ in range(_NUM_ITERATIONS)]
            # Compare sequences
            self.assertListEqual(first, second)

    def test_randrange(self):
        # Create a seed and a prng
        seed = _random_seed()
        rng = dax.util.random.PCG_XSH_RR(None)

        with _suppress_warnings():
            for start, stop in [(0, 100), (0, 1), (-20, 20), (-1000, -1), (-2**31, 2**31 - 1)]:
                # Create a first sequence
                rng.init(seed)
                first = [rng.randrange(start, stop) for _ in range(_NUM_ITERATIONS)]
                # Check first sequence
                self.assertTrue(all(isinstance(r, np.int32) for r in first))
                self.assertTrue(all(start <= r < stop for r in first))
                # Create a second sequence
                rng.init(seed)
                second = [rng.randrange(start, stop) for _ in range(_NUM_ITERATIONS)]
                # Compare sequences
                self.assertListEqual(first, second)

    def test_randfloat(self):
        # Create a seed and a prng
        seed = _random_seed()
        rng = dax.util.random.PCG_XSH_RR(None)

        with _suppress_warnings():
            # Create a first sequence
            rng.init(seed)
            first = [rng.randfloat() for _ in range(_NUM_ITERATIONS)]
            # Check first sequence
            self.assertTrue(all(isinstance(r, float) for r in first))
            self.assertTrue(all(0.0 <= r < 1.0 for r in first))
            # Create a second sequence
            rng.init(seed)
            second = [rng.randfloat() for _ in range(_NUM_ITERATIONS)]
            # Compare sequences
            self.assertListEqual(first, second)
