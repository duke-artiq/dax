import pytest
import sys
from typing import Any, Generator

from dax.util.artiq import get_managers

from test.dummy_driver import DummyDriver
from test.environment import CI_ENABLED
from test.network import find_free_port


@pytest.fixture
def managers(request: pytest.FixtureRequest) -> Generator[Any, None, None]:
    """
    Fixture to set up and yield managers based on the provided device_db.

    Args:
        request (pytest.FixtureRequest): The pytest request object that provides access to the parametrized values.

    Yields:
        Any: The managers set up with the provided device_db.

    Examples:
        The below provide simple example on how to create a test with parametrize device_db.

        ```
        @pytest.mark.parametrize("device_db", [
            {
                "lca": {
                    "type": "local",
                    "module": "dax.util.controller",
                    "class": "LocalControllerAdapter",
                    "arguments": {
                        "controller_module": "test.dummy_driver", # module where the driver class is define
                        "controller_class": "DummyDriver", # class name for the driver
                        "arguments": {"simulation": True}, # argument for the constructor
                        "is_context": False,
                    }
                },
            },
            {
                "sca": {
                    "type": "local",
                    "module": "dax.util.controller",
                    "class": "SubprocessControllerAdapter",
                    "arguments": {
                        "port": 3000,
                        "command": "python -m test.dummy_driver -p {port} --bind {bind}",
                    }
                }
            },
        ], indirect=True)
        def test_artiq_env(managers):
            artiq_env : HasEnvironment = EnvClass(managers)
            assert artiq_env is not None
            # call artiq_env methods
            # Add more assertions as needed
        ```

        The same thing can be done using artiq.EnvExperiment.
    """
    device_db = request.param  # type: ignore[attr-defined]

    # do something before setting up the managers
    # ...

    # setup managers
    managers = get_managers(device_db=device_db)
    yield managers

    # do something after using the managers
    # ...


def _run_controller_adapter_test(managers):
    # Unpack managers
    device_mgr, _, _, _ = managers
    # Get ddb
    ddb = device_mgr.get_device_db()
    assert ddb
    # Get devices
    for device_name in ddb:
        device = device_mgr.get(device_name)
        assert device.ping()


@pytest.mark.parametrize("managers", [
    {
        "lca": {
            "type": "local",
            "module": "dax.util.controller",
            "class": "LocalControllerAdapter",
            "arguments": {
                "controller_module": DummyDriver.__module__,
                "controller_class": DummyDriver.__name__,
                "arguments": {"simulation": False},
                "is_context": False,
            }
        }
    },
    {
        "lca": {
            "type": "local",
            "module": "dax.util.controller",
            "class": "LocalControllerAdapter",
            "arguments": {
                "controller_module": DummyDriver.__module__,
                "controller_class": DummyDriver.__name__,
                "arguments": {"simulation": True},
                "is_context": False,
            }
        }
    },
], indirect=True)
def test_local_controller_adapter(managers):
    _run_controller_adapter_test(managers)


@pytest.mark.parametrize("managers", [
    {
        "sca": {
            "type": "local",
            "module": "dax.util.controller",
            "class": "SubprocessControllerAdapter",
            "arguments": {
                "host": "127.0.0.1",  # Run on IPv4 for testing in docker
                "port": find_free_port(),
                "command": f"{sys.executable} -m {DummyDriver.__module__} -p {{port}} --bind {{bind}} "
                           "--no-localhost-bind",
            }
        }
    },
], indirect=True)
@pytest.mark.skipif(not CI_ENABLED, reason='Not in a CI environment, skipping slow test')
def test_subprocess_controller_adapter(managers):
    _run_controller_adapter_test(managers)
