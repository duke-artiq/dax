import argparse
from sipyco.pc_rpc import simple_server_loop
from sipyco.common_args import bind_address_from_args, simple_network_args


class DummyDriver:
    def __init__(self, *, simulation=False):
        """Initialize a dummy driver.

        :param simulation: Simulation flag should come from a test parameter to be able to run test on real device
        """
        self._simulation = simulation

    def ping(self):
        return True

    def close(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, *_):
        pass

    def is_simulation(self):
        return self._simulation


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--simulation", "-s", action="store_true")
    simple_network_args(parser, default_port=3000)
    args = parser.parse_args()

    with DummyDriver(simulation=args.simulation) as driver:
        simple_server_loop({"dummy_driver": driver}, bind_address_from_args(args), args.port)


if __name__ == "__main__":
    main()
