import typing
import unittest
import unittest.mock
import numpy as np

import artiq.coredevice.edge_counter
import artiq.language.units

from dax.experiment import *
from dax.interfaces.detection import DetectionInterface
from dax.modules.cool_context import CoolContextError, CoolContext, CoolContextCounter
from dax.util.artiq import get_managers

import test.helpers


class _CoolContextSystem(DaxSystem):
    SYS_ID = 'unittest_system'
    SYS_VER = 0
    CORE_LOG_KEY = None
    DAX_INFLUX_DB_KEY = None

    CONTEXT_CLS = CoolContext
    NUM_CHANNELS = 5

    def build(self, state_detection_threshold=2, **kwargs) -> None:  # type: ignore[override]
        super().build()
        self.detection = _MockDetectionModule(self, 'detection',
                                              state_detection_threshold=state_detection_threshold,
                                              num_channels=self.NUM_CHANNELS)
        self.cool_context = self.CONTEXT_CLS(self, 'cool_context', **kwargs)


class CoolContextTestCase(unittest.TestCase):
    SYSTEM_CLS = _CoolContextSystem

    def setUp(self) -> None:
        self.managers = get_managers()
        self.s = self.SYSTEM_CLS(self.managers)
        self.s.dax_init()
        self.c = self.s.cool_context

    def tearDown(self) -> None:
        # Close managers
        self.managers.close()

    def test_in_context(self):
        # Initially we are out of context
        self.assertFalse(self.c.in_context(), 'in_context() reported wrong value')
        with self.c:
            # In context
            self.assertTrue(self.c.in_context(), 'in_context() reported wrong value')

        # Out of context
        self.assertFalse(self.c.in_context(), 'in_context() reported wrong value')
        # Open context manually
        self.c.open()
        # In context
        self.assertTrue(self.c.in_context(), 'in_context() reported wrong value')
        # Close context manually
        self.c.close()
        # Out of context
        self.assertFalse(self.c.in_context(), 'in_context() reported wrong value')

    def test_append_out_of_context(self):
        for err_type in [CoolContextError]:
            # We can not call append out of context
            with self.assertRaises(err_type, msg='Append out of context did not raise'):
                self.c.append([1])

    def test_nesting_exceptions(self):
        for err_type in [CoolContextError]:
            with self.assertRaises(err_type, msg='Close out of context did not raise'):
                self.c.close()
            # Open the context
            self.c.open()
            with self.assertRaises(err_type, msg='Open context in context did not raise'):
                self.c.open()
            # Close the context
            self.c.close()
            with self.assertRaises(err_type, msg='Close out of context did not raise'):
                self.c.close()
            with self.c:
                with self.assertRaises(err_type, msg='Nesting context did not raise'):
                    with self.c:
                        pass

    def test_append(self):
        data = [
            [1, 9],
            [2, 8],
            [2, 7],
            [3, 6],
            [3, 5],
            [3, 4],
        ]

        with self.c:
            # Check buffer
            self.assertListEqual([], self.c._buffer, 'Buffer was not cleared when entering new context')
            for d in data:
                self.c.append(d)
                self.assertEqual(d, self.c._buffer[-1], 'Append did not appended data to buffer')
            # Check buffer
            self.assertListEqual(data, self.c._buffer, 'Buffer did not contain expected data')

        with self.c:
            # Check buffer
            self.assertListEqual([], self.c._buffer, 'Buffer was not cleared when entering new context')

    def test_append_2(self):
        test_data = [
            [[1, 9], [2, 8], [2, 7]],
            [[3, 6], [3, 5], [3, 4]],
        ]

        for data in test_data:
            with self.c:
                # Check buffer
                self.assertListEqual([], self.c._buffer, 'Buffer was not cleared when entering new context')
                for d in data:
                    self.c.append(d)
                    self.assertEqual(d, self.c._buffer[-1], 'Append did not appended data to buffer')
                # Check buffer
                self.assertListEqual(data, self.c._buffer, 'Buffer did not contain expected data')

    def test_empty_data(self):
        data = [[], [], []]

        # Open context manually
        self.c.open()
        # Check buffer
        self.assertListEqual([], self.c._buffer, 'Buffer was not cleared when entering new context')
        for d in data:
            self.c.append(d)
            self.assertEqual(d, self.c._buffer[-1], 'Append did not appended data to buffer')
        # Check buffer
        self.assertListEqual(data, self.c._buffer, 'Buffer did not contain expected data')
        with self.assertRaises(ValueError, msg='Submitting empty data did not raise'):
            self.c.close()

    def test_inconsistent_data(self):
        data = [[1, 2], [3, 4], [5, 6, 7]]

        # Open context manually
        self.c.open()
        # Check buffer
        self.assertListEqual([], self.c._buffer, 'Buffer was not cleared when entering new context')
        for d in data:
            self.c.append(d)
            self.assertEqual(d, self.c._buffer[-1], 'Append did not appended data to buffer')
        # Check buffer
        self.assertListEqual(data, self.c._buffer, 'Buffer did not contain expected data')
        with self.assertRaises(ValueError, msg='Submitting inconsistent data did not raise'):
            self.c.close()

    def test_get_mean_counts(self):
        num_histograms = 8
        data = [
            [1, 9],
            [2, 9],
            [2, 9],
            [3, 8],
            [4, 7],
            [5, 6],
            [6, 5],
            [7, 2],
            [8, 1],
            [9, 0],
        ]
        mean_count_ref = [np.mean(c) for c in zip(*data)]

        self.c.clear_mean_count_plot()
        for _ in range(num_histograms):
            with self.c:
                for d in data:
                    self.c.append(d)

        # Check data
        mean_counts = np.asarray(self.c.get_dataset(self.c._mean_count_plot_key, archive=False)).transpose()
        for counts, ref in zip(mean_counts, mean_count_ref):
            # This test fails because in simulation the append_to_dataset() method appends double!
            # self.assertEqual(len(counts), num_histograms)
            for c in counts:
                self.assertAlmostEqual(c, ref, msg='Obtained mean count does not match reference')

    def test_get_stdev_counts(self):
        num_histograms = 8
        data = [
            [1, 9],
            [2, 9],
            [2, 9],
            [3, 8],
            [4, 7],
            [5, 6],
            [6, 5],
            [7, 2],
            [8, 1],
            [9, 0],
        ]
        stdev_count_ref = [np.std(c) for c in zip(*data)]

        self.c.clear_mean_count_plot()
        for _ in range(num_histograms):
            with self.c:
                for d in data:
                    self.c.append(d)

        # Check data
        stdev_counts = np.asarray(self.c.get_dataset(self.c._stdev_count_plot_key, archive=False)).transpose()
        for counts, ref in zip(stdev_counts, stdev_count_ref):
            # This test fails because in simulation the append_to_dataset() method appends double!
            # self.assertEqual(len(counts), num_histograms)
            for c in counts:
                self.assertAlmostEqual(c, ref, msg='Obtained stdev count does not match reference')

    def test_applets(self):
        def plot_fn():
            # In simulation, we can only call these functions, but nothing will happen
            self.c.plot_histogram()
            self.c.plot_mean_count()
            self.c.clear_mean_count_plot()
            self.c.disable_histogram_plot()
            self.c.disable_mean_count_plot()
            self.c.disable_all_plots()

        plot_fn()

        # Add data to the archive
        num_histograms = 4
        data = [
            [4, 1, 0],
            [5, 2, 0],
            [6, 3, 0],
            [7, 4, 0],
        ]

        # Store data
        for _ in range(num_histograms):
            with self.c:
                for d in data:
                    self.c.append(d)

        plot_fn()

    def test_kernel_invariants(self):
        # Test kernel invariants
        test.helpers.test_system_kernel_invariants(self, self.s)


class _MockDetectionModule(DaxModule, DetectionInterface):

    def build(self, state_detection_threshold: int, num_channels: int):  # type: ignore[override]
        self.state_detection_threshold = state_detection_threshold
        self.num_channels = num_channels

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass

    def get_pmt_array(self) -> typing.List[artiq.coredevice.edge_counter.EdgeCounter]:
        return [unittest.mock.Mock(artiq.coredevice.edge_counter.EdgeCounter) for _ in range(self.num_channels)]

    def get_state_detection_threshold(self) -> int:
        return self.state_detection_threshold

    def get_default_detection_time(self) -> float:
        return 100 * us


class _CoolContextCounterSystem(_CoolContextSystem):
    CONTEXT_CLS = CoolContextCounter

    def build(self, auto_reset=True, **kwargs) -> None:  # type: ignore[override]
        super().build(auto_reset=auto_reset, **kwargs)


class CoolContextCounterTestCase(CoolContextTestCase):
    SYSTEM_CLS = _CoolContextCounterSystem

    TEST_DATA = [
        [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 1, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 9],
        [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 2, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 8],
        [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 2, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 7],
        [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 6],
        [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 5],
        [CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 3, 0, CoolContextCounter.DEFAULT_COUNT_THRESHOLD + 4],
    ]

    def _append_data(self):
        with self.c:
            # Check that counters are reset
            counters_ref = [0] * self.SYSTEM_CLS.NUM_CHANNELS
            self.assertListEqual(self.c._counter, counters_ref)
            for i, d in enumerate(self.TEST_DATA, start=1):
                # Append
                self.c.append(d)
                # Check counters
                counters_ref[1] = i
                self.assertListEqual(self.c._counter, counters_ref)

    def test_reset_counters(self):
        # Append data (with assertions included)
        self._append_data()
        # Append data again to ensure values are automatically reset
        self._append_data()
        # Now manually reset counters
        self.c.reset_counters()
        self.assertListEqual(self.c._counter, [0] * self.SYSTEM_CLS.NUM_CHANNELS)

    def test_get_num_lost_ions(self):
        # Append data
        self._append_data()
        # Test ion loss detection
        num_violations = len(self.TEST_DATA)
        self.assertEqual(self.c.get_num_lost_ions(num_violations - 1), 1)
        self.assertEqual(self.c.get_num_lost_ions(num_violations), 1)
        self.assertEqual(self.c.get_num_lost_ions(num_violations + 1), 0)
