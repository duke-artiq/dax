import unittest

import dax.util.test.coredevice
from test.environment import CI_ENABLED

__all__ = ['CoredeviceCompileTestCase']


@unittest.skipUnless(CI_ENABLED, 'Not in a CI environment, skipping compilation test')
class CoredeviceCompileTestCase(dax.util.test.coredevice.CompileTestCase):
    pass
