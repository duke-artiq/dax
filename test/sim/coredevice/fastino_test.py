import random
import typing

from artiq.experiment import *

import dax.sim.test_case
import dax.sim.coredevice.fastino

import test.sim.coredevice._compile_testcase as compile_testcase
from test.environment import CI_ENABLED

_NUM_SAMPLES = 1000 if CI_ENABLED else 100


def _create_ddb(log2_width):
    return {
        'core': {
            'type': 'local',
            'module': 'artiq.coredevice.core',
            'class': 'Core',
            'arguments': {'host': None, 'ref_period': 1e-9}
        },
        "dut": {
            "type": "local",
            "module": "artiq.coredevice.fastino",
            "class": "Fastino",
            "arguments": {"channel": 0x10, "log2_width": log2_width},
        },
    }


class _Environment(HasEnvironment):
    def build(self):
        self.core = self.get_device('core')
        self.dut = self.get_device('dut')


class FastinoTestCase(dax.sim.test_case.PeekTestCase):
    SEED = None
    LOG2_WIDTH = 0
    _NUM_CHANNELS = dax.sim.coredevice.fastino.Fastino._NUM_CHANNELS

    def setUp(self) -> None:
        self.rng = random.Random(self.SEED)
        self.env = self.construct_env(_Environment, device_db=_create_ddb(self.LOG2_WIDTH))

    def test_conversion(self):
        for _ in range(_NUM_SAMPLES):
            v = self.rng.uniform(-10.0, 9.99)
            with self.subTest(v=v):
                o = dax.sim.coredevice.fastino._mu_to_voltage(self.env.dut.voltage_to_mu(v))
                self.assertAlmostEqual(v, o, places=3)

    def _test_uninitialized(self):
        for name in ['init', 'afe_power_down', 'cic_config']:
            self.expect(self.env.dut, name, 'x')
        for name in ['continuous', 'cic']:
            self.expect(self.env.dut, name, 'x')
        self.expect(self.env.dut, 'led', 'x')
        for i in range(self._NUM_CHANNELS):
            self.expect(self.env.dut, f'v_out_{i}', 'x')

    def test_init(self):
        self._test_uninitialized()
        self.env.dut.init()
        self.expect(self.env.dut, 'init', 1)
        self.expect(self.env.dut, 'afe_power_down', 0)
        self.expect(self.env.dut, 'continuous', '0' * dax.sim.coredevice.fastino.Fastino._NUM_CHANNELS)
        self.expect(self.env.dut, 'cic', '1' * dax.sim.coredevice.fastino.Fastino._NUM_CHANNELS)
        self.expect(self.env.dut, 'cic_config', 0)
        self.expect(self.env.dut, 'led', '0' * dax.sim.coredevice.fastino.Fastino._NUM_LEDS)
        for i in range(self._NUM_CHANNELS):
            self.expect(self.env.dut, f'v_out_{i}', 'x')

    def test_dac_clear(self):
        self._test_uninitialized()
        self.env.dut.set_cfg(dac_clr=1)
        for i in range(self._NUM_CHANNELS):
            self.expect(self.env.dut, f'v_out_{i}', 0.0)

    def test_afe_power_down(self):
        self._test_uninitialized()
        self.env.dut.set_cfg(afe_power_down=1)
        self.expect(self.env.dut, 'afe_power_down', 1)

    def test_apply_cic(self):
        self.expect(self.env.dut, 'cic', 'x')  # Equivalent to SignalNotSet

        ref = '0' * (self._NUM_CHANNELS - 1) + '1' + '0' * (self._NUM_CHANNELS - 1)
        for i in range(self._NUM_CHANNELS):
            self.env.dut.apply_cic(0x1 << i)
            value = ref[i:self._NUM_CHANNELS + i]
            assert value[-1 - i] == '1'
            self.expect(self.env.dut, 'cic', value)
            self.env.dut.apply_cic(0x0)
            self.expect(self.env.dut, 'cic', '0' * self._NUM_CHANNELS)

    def test_leds(self):
        self.expect(self.env.dut, 'led', 'x')  # Equivalent to SignalNotSet

        ref = '0' * (self.env.dut._NUM_LEDS - 1) + '1' + '0' * (self.env.dut._NUM_LEDS - 1)
        for i in range(self.env.dut._NUM_LEDS):
            self.env.dut.set_leds(0x1 << i)
            value = ref[i:self.env.dut._NUM_LEDS + i]
            assert value[-1 - i] == '1'
            self.expect(self.env.dut, 'led', value)
            self.env.dut.set_leds(0x0)
            self.expect(self.env.dut, 'led', '0' * self.env.dut._NUM_LEDS)

    def test_set_dac(self):
        self._test_uninitialized()
        self.env.dut.set_hold(0)  # Disable hold for all channels
        for _ in range(_NUM_SAMPLES):
            # Generate parameters
            v = self.rng.uniform(-10.0, 9.99)
            c = self.rng.randrange(self._NUM_CHANNELS)
            with self.subTest(v=v, c=c):
                # Set DAC, which updates immediately because hold is disabled
                self.env.dut.set_dac(c, v)
                # Test
                self.expect_close(self.env.dut, f'v_out_{c}', v, places=3)

    def test_set_dac_hold(self):
        self._test_uninitialized()
        self.env.dut.set_hold((1 << self._NUM_CHANNELS) - 1)  # Hold all channels
        for c in range(self._NUM_CHANNELS):
            # Generate voltage
            v = self.rng.uniform(-10.0, 9.99)
            with self.subTest(v=v, c=c):
                # Set DAC
                self.expect(self.env.dut, f'v_out_{c}', 'x')  # SignalNotSet
                self.env.dut.set_dac(c, v)
                self.expect(self.env.dut, f'v_out_{c}', 'x')  # SignalNotSet
                # Update and test
                self.env.dut.update(1 << c)
                self.expect_close(self.env.dut, f'v_out_{c}', v, places=3)

    def test_set_continuous(self):
        self._test_uninitialized()
        for _ in range(_NUM_SAMPLES):
            # Generate parameters
            mask = self.rng.randrange((1 << self._NUM_CHANNELS) - 1)
            with self.subTest(mask=mask):
                # Set continuous
                self.env.dut.set_continuous(mask)
                # Test
                self.expect(self.env.dut, 'continuous', f'{mask:0{self._NUM_CHANNELS}b}')

    def test_set_group(self):
        self.env.dut.set_hold(0)  # Disable hold for all channels
        with self.assertRaises(AssertionError):
            self.env.dut.set_group(0, [])

    def test_set_group_hold(self):
        self.env.dut.set_hold((1 << self._NUM_CHANNELS) - 1)  # Hold all channels
        with self.assertRaises(AssertionError):
            self.env.dut.set_group(0, [])


class FastinoWideTestCase(FastinoTestCase):
    LOG2_WIDTH = 5  # Full width

    def test_set_dac(self):
        self.env.dut.set_hold(0)  # Disable hold for all channels
        with self.assertRaises(AssertionError):
            self.env.dut.set_dac(0, 0.0)

    def test_set_dac_hold(self):
        self.env.dut.set_hold((1 << self._NUM_CHANNELS) - 1)  # Hold all channels
        with self.assertRaises(AssertionError):
            self.env.dut.set_dac(0, 0.0)

    def test_set_group(self):
        self._test_uninitialized()
        self.env.dut.set_hold(0)  # Disable hold for all channels
        for _ in range(_NUM_SAMPLES):
            # Generate parameters
            group = [self.rng.uniform(-10.0, 9.99) for _ in range(self._NUM_CHANNELS)]
            with self.subTest(group=group):
                # Set group, which updates immediately because hold is disabled
                self.env.dut.set_group(0, group)
                # Test
                for c, v in enumerate(group):
                    self.expect_close(self.env.dut, f'v_out_{c}', v, places=3)

    def test_set_group_hold(self):
        self._test_uninitialized()
        self.env.dut.set_hold((1 << self._NUM_CHANNELS) - 1)  # Hold all channels
        # Generate parameters
        group = [self.rng.uniform(-10.0, 9.99) for _ in range(self._NUM_CHANNELS)]
        # Set group
        for c in range(self._NUM_CHANNELS):
            self.expect(self.env.dut, f'v_out_{c}', 'x')  # SignalNotSet
        self.env.dut.set_group(0, group)
        for c in range(self._NUM_CHANNELS):
            self.expect(self.env.dut, f'v_out_{c}', 'x')  # SignalNotSet
        # Update and test
        self.env.dut.update((1 << self._NUM_CHANNELS) - 1)
        for c, v in enumerate(group):
            self.expect_close(self.env.dut, f'v_out_{c}', v, places=3)


class CompileTestCase(compile_testcase.CoredeviceCompileTestCase):
    DEVICE_DB = _create_ddb(FastinoTestCase.LOG2_WIDTH)
    DEVICE_CLASS: typing.ClassVar[typing.Type] = dax.sim.coredevice.fastino.Fastino
    DEVICE_KWARGS = DEVICE_DB['dut']['arguments']
    FN_KWARGS: typing.ClassVar[typing.Dict[str, typing.Dict[str, typing.Any]]] = {
        'write': {'addr': 0, 'data': 0},
        'set_dac_mu': {'dac': 0, 'data': 0},
        'set_group_mu': {'dac': 0, 'data': [0]},
        'write_dac_mu': {'channel': 0, 'value': 0},
        'voltage_to_mu': {'voltage': 0.0},
        'voltage_group_to_mu': {'voltage': [0.0], 'data': [0]},
        'set_dac': {'dac': 0, 'voltage': 0.0},
        'set_group': {'dac': 0, 'voltage': [0.0]},
        'update': {'update': 0},
        'set_hold': {'hold': 0},
        'set_leds': {'leds': 0},
        'set_continuous': {'channel_mask': 0},
        'stage_cic_mu': {'rate_mantissa': 0, 'rate_exponent': 0, 'gain_exponent': 0},
        'stage_cic': {'rate': 1},
        'apply_cic': {'channel_mask': 0},
    }


class CompileWideTestCase(CompileTestCase):
    DEVICE_DB = _create_ddb(FastinoWideTestCase.LOG2_WIDTH)
    DEVICE_KWARGS = DEVICE_DB['dut']['arguments']
