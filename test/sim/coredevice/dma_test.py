import numpy as np
import unittest

import dax.sim.coredevice.dma
from dax.sim import enable_dax_sim
from dax.util.artiq import get_managers

import test.sim.coredevice._compile_testcase as compile_testcase


_DMA_NAME = "foo"


class CoreDMATestCase(unittest.TestCase):

    _DEVICE_DB = enable_dax_sim({
        'core': {
            'type': 'local',
            'module': 'artiq.coredevice.core',
            'class': 'Core',
            'arguments': {'host': None, 'ref_period': 1e-9}
        },
        "core_dma": {
            "type": "local",
            "module": "artiq.coredevice.dma",
            "class": "CoreDMA"
        },
    }, enable=True, output='null', moninj_service=False)

    def setUp(self) -> None:
        self.managers = get_managers(device_db=self._DEVICE_DB)
        self.dma = dax.sim.coredevice.dma.CoreDMA(self.managers.device_mgr, _key='core_dma')

    def tearDown(self) -> None:
        # Close managers
        self.managers.close()

    def test_record(self, name=_DMA_NAME):
        # Record
        with self.dma.record(name):
            pass

        # Record a second time with the same name should pass
        with self.dma.record(name):
            pass

    def test_erase(self, name=_DMA_NAME):
        # Erasing a non-existing name should pass
        self.dma.erase(name)

        # Record
        with self.dma.record(name):
            pass

        # Erase again
        self.dma.erase(name)
        self.dma.erase(name)  # Erasing a non-existing name should pass

    def test_playback(self, name=_DMA_NAME):
        # Play without recording raises a KeyError in simulation
        with self.assertRaises(KeyError):
            self.dma.playback(name)

        # Record
        with self.dma.record(name):
            pass

        # Playback
        self.dma.playback(name)

    def test_get_handle(self, name=_DMA_NAME):
        # Get handle without recording raises a KeyError in simulation
        with self.assertRaises(KeyError):
            self.dma.get_handle(name)

        # Record
        with self.dma.record(name):
            pass

        # Get handle
        self.assertIsNotNone(self.dma.get_handle(name))

    def test_playback_handle(self, name=_DMA_NAME):
        # Record
        with self.dma.record(name):
            pass

        # Get handle
        handle = self.dma.get_handle(name)
        # Playback
        self.dma.playback_handle(handle)

    def test_playback_handle_dma_error(self, name=_DMA_NAME):
        # Record
        with self.dma.record(name):
            pass

        # Get handle
        handle = self.dma.get_handle(name)
        # Play handle
        self.dma.playback_handle(handle)
        # Chane epoch
        with self.dma.record(name):
            pass
        # Playback invalid handle
        with self.assertRaises(dax.sim.coredevice.dma.DMAError):
            self.dma.playback_handle(handle)

        # Get handle
        handle = self.dma.get_handle(name)
        # Play handle
        self.dma.playback_handle(handle)
        # Change epoch
        self.dma.erase(name)
        # Playback invalid handle
        with self.assertRaises(dax.sim.coredevice.dma.DMAError):
            self.dma.playback_handle(handle)


class CompileTestCase(compile_testcase.CoredeviceCompileTestCase):
    DEVICE_CLASS = dax.sim.coredevice.dma.CoreDMA
    FN_KWARGS = {
        'record': {'name': _DMA_NAME},
        'erase': {'name': _DMA_NAME},
        'playback': {'name': _DMA_NAME},
        'get_handle': {'name': _DMA_NAME},
        'playback_handle': {'handle': (0, np.int64(0), 0)},
    }
