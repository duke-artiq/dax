{ buildPythonPackage
, fetchFromGitHub
, numpy
, scipy
}:

buildPythonPackage {
  pname = "nistrng";
  version = "1.2.3";
  src = fetchFromGitHub {
    owner = "InsaneMonster";
    repo = "NistRng";
    rev = "93e2e991f148b39fa4ef1f5f3ca045ef11e4b2fe";
    sha256 = "sha256-x1LO6PNOevCaQ7GZNxXiTFwQ+goIYcnxThKHYpoSzzE=";
  };

  nativeBuildInputs = [
    numpy
    scipy
  ];
}
