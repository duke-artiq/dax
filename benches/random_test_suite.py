#!/usr/bin/env python3

"""
This file is based on example code found in https://github.com/InsaneMonster/NistRng.

Note that the value of the nistrng library was not evaluated,
and therefore it is unknown what significance its results have.
"""

import argparse
import enum
import numpy as np
import random

from dax import __version__ as __dax_version__
from dax.util.git import in_repository, get_repository_info
from dax.util.random import PCG_XSH_RR

import nistrng


def _suppress_warnings():
    sup = np.testing.suppress_warnings()
    sup.filter(RuntimeWarning, "overflow encountered in .*")
    return sup


@enum.unique
class PRNG(enum.Enum):
    FAIL = enum.auto()
    PCG_XSH_RR = enum.auto()
    PYTHON = enum.auto()
    NUMPY = enum.auto()


@enum.unique
class PCGMethods(enum.Enum):
    RANDINT32 = enum.auto()
    RANDINT64 = enum.auto()
    RANDRANGE = enum.auto()


def run_test_suite(prng, seed, count, increment, pcg_method):
    print(f"DAX version: {__dax_version__}")
    if in_repository():
        repo = get_repository_info()
        print(f"DAX commit: {repo.commit} (dirty={repo.dirty})")
    print(f"Running DAX test suite with prng {prng.name}, seed {seed}, count {count}")
    if prng is PRNG.PCG_XSH_RR:
        print(f"Using PCG_XSH_RR with increment {increment}, method {pcg_method.name}")

    if prng is PRNG.FAIL:
        # Generate a sequence of zeros
        sequence = np.zeros(count, dtype=np.uint8)
        print("WARNING: test is configured to fail")
    elif prng is PRNG.PCG_XSH_RR:
        # Create and initialize PCG_XSH_RR class and generate random numbers
        rnd = PCG_XSH_RR(None, increment=increment)
        with _suppress_warnings():
            rnd.init(random.Random().randrange(-2**63, 2**63 - 1) if seed is None else seed)
            if pcg_method is PCGMethods.RANDINT32:
                sequence = np.asarray(
                    [rnd.randint32() for _ in range(-(count // -4))],
                    dtype=np.int32
                ).view(np.uint8)[:count]
            elif pcg_method is PCGMethods.RANDINT64:
                sequence = np.asarray(
                    [rnd.randint64() for _ in range(-(count // -8))],
                    dtype=np.int64
                ).view(np.uint8)[:count]
            elif pcg_method is PCGMethods.RANDRANGE:
                sequence = np.asarray([rnd.randrange(0, 2**8) for _ in range(count)], dtype=np.uint8)
            else:
                raise ValueError(f"{pcg_method} not implemented")
    elif prng is PRNG.PYTHON:
        # Create Random class and generate random numbers
        rnd = random.Random(seed)
        sequence = [rnd.randrange(2**8) for _ in range(count)]
    elif prng is PRNG.NUMPY:
        # Create NumPy PCG64 random class and generate random numbers
        rnd = np.random.Generator(np.random.PCG64(seed))
        sequence = [rnd.integers(2**8, endpoint=False, dtype=np.uint8) for _ in range(count)]
    else:
        raise ValueError(f"{prng} not implemented")

    # Pack the sequence of random integers in an 8-bit representation
    binary_sequence = nistrng.pack_sequence(np.asarray(sequence, dtype=np.int32))
    assert len(binary_sequence) == count * 8

    # Check the eligibility of the test and generate an eligible battery from the default NIST-sp800-22r1a battery
    eligible_battery = nistrng.check_eligibility_all_battery(binary_sequence, nistrng.SP800_22R1A_BATTERY)
    print(f"Eligible test from NIST-SP800-22r1a: {', '.join(eligible_battery.keys())}")

    # Test the sequence on the eligible tests
    results = nistrng.run_all_battery(binary_sequence, eligible_battery, check_eligibility=False)
    # Print results
    for result, elapsed_time in results:
        print(f"Test {result.name}:", "PASSED" if result.passed else "FAILED")
        print(f"- score: {np.round(result.score, 3)}")
        print(f"- elapsed time: {elapsed_time} ms")


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Run a PRNG test suite')
    parser.add_argument('--prng', choices=[prng.name.lower() for prng in PRNG], default=PRNG.PCG_XSH_RR.name.lower(),
                        help='the PRNG to test (default: %(default)s)')
    parser.add_argument('--seed', default=None, type=int,
                        help='seed for the PRNG (random seed by default)')
    parser.add_argument('--count', default=1000, type=int,
                        help='number of random bytes to test (nistrng chosen default: %(default)s)')
    parser.add_argument('--increment', default=PCG_XSH_RR.DEFAULT_INCREMENT, type=int,
                        help='increment used by PCG_XSH_RR (default: %(default)s)')
    parser.add_argument('--pcg-method', choices=[method.name.lower() for method in PCGMethods],
                        default=PCGMethods.RANDINT32.name.lower(),
                        help='method used by PCG_XSH_RR (default: %(default)s)')
    args = parser.parse_args()

    # Run test suite
    run_test_suite(
        PRNG[args.prng.upper()],
        args.seed,
        args.count,
        increment=args.increment,
        pcg_method=PCGMethods[args.pcg_method.upper()]
    )


if __name__ == '__main__':
    main()
