#!/usr/bin/env python3

import abc
import argparse
import random
import timeit

from artiq import __version__ as __artiq_version__
import artiq.coredevice.ttl

from dax import __version__ as __dax_version__
from dax.experiment import *
from dax.sim.test_case import PeekTestCase
from dax.util.git import in_repository, get_repository_info


# Device DB
_DEVICE_DB = {
    # Core devices
    'core': {
        'type': 'local',
        'module': 'artiq.coredevice.core',
        'class': 'Core',
        'arguments': {'host': None, 'ref_period': 1e-9}
    },
    'core_cache': {
        'type': 'local',
        'module': 'artiq.coredevice.cache',
        'class': 'CoreCache'
    },
    'core_dma': {
        'type': 'local',
        'module': 'artiq.coredevice.dma',
        'class': 'CoreDMA'
    },

    # TTL devices
    'ttl0': {
        'type': 'local',
        'module': 'artiq.coredevice.ttl',
        'class': 'TTLOut',
        'arguments': {},
    },
}


class _BenchmarkSystem(DaxSystem):
    SYS_ID = 'benchmark_system'
    SYS_VER = 0
    CORE_LOG_KEY = None
    DAX_INFLUX_DB_KEY = None

    def build(self, *args, **kwargs):
        super().build(*args, **kwargs)
        self._ttl = self.get_device('ttl0', artiq.coredevice.ttl.TTLInOut)


class _Benchmark(PeekTestCase, abc.ABC):
    def setUp(self, iterations, events, step, seed=None, **kwargs):
        self._iterations = iterations
        self._events = events
        self._step = step
        self._rng = random.Random(seed)
        self.system = self.construct_env(_BenchmarkSystem, device_db=_DEVICE_DB, build_kwargs=kwargs)

    def sequential_insertion(self, iterations):
        time_mu = now_mu()
        for _ in range(iterations):
            time_mu += self._step
            at_mu(time_mu)
            self.system._ttl.on()

    @abc.abstractmethod
    def run(self):
        pass


class _SequentialInsertion(_Benchmark):
    def run(self):
        self.sequential_insertion(self._iterations)


class _RandomInsertion(_Benchmark):
    def setUp(self, *args, **kwargs):
        super().setUp(*args, **kwargs)
        max_time_mu = self._iterations ** 2
        self._t = [self._rng.randrange(max_time_mu) for _ in range(self._iterations)]

    def run(self):
        for t in self._t:
            at_mu(t)
            self.system._ttl.on()


class _TailPeek(_Benchmark):
    def setUp(self, *args, **kwargs):
        super().setUp(*args, **kwargs)
        self.sequential_insertion(self._events)

    def run(self):
        for _ in range(self._iterations):
            self.peek(self.system._ttl, 'state')


class _RandomPeek(_Benchmark):
    def setUp(self, *args, **kwargs):
        super().setUp(*args, **kwargs)
        self.sequential_insertion(self._events)
        max_time_mu = now_mu()
        self._t = [self._rng.randrange(max_time_mu) for _ in range(self._iterations)]

    def run(self):
        for t in self._t:
            at_mu(t)
            self.peek(self.system._ttl, 'state')


def run_benchmarks(iterations, events, step, seed, precision):
    print(f"ARTIQ version: {__artiq_version__}")
    print(f"DAX version: {__dax_version__}")
    if in_repository():
        repo = get_repository_info()
        print(f"DAX commit: {repo.commit} (dirty={repo.dirty})")
    print(f"running DAX.sim peek benchmark with {iterations} iterations, {events} events, step {step}, seed {seed}")

    for benchmark_cls in [_SequentialInsertion, _RandomInsertion, _TailPeek, _RandomPeek]:
        # Construct the benchmark
        print(f"{benchmark_cls.__name__[1:]}: ", end="")
        benchmark = benchmark_cls()
        benchmark.setUp(iterations, events, step, seed=seed)

        # Record start time
        t_start = timeit.default_timer()
        # Run the benchmark
        benchmark.run()
        # Record total time
        t = timeit.default_timer() - t_start

        # Report
        print(f"{t*1e6/iterations:.{precision:d}f} us")


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Benchmark DAX.sim peek backend performance')
    parser.add_argument('--iterations', default=100000, type=int,
                        help='number of iterations for each test (default: %(default)s)')
    parser.add_argument('--events', default=None, type=int,
                        help='number of events in memory for peek benchmarks (default: number of iterations)')
    parser.add_argument('--step', default=1000, type=int,
                        help='step size used for event spacing (default: %(default)s)')
    parser.add_argument('--seed', default=None, type=int,
                        help='seed for the PRNG')
    parser.add_argument('--precision', default=5, type=int,
                        help='precision used for printing the time (default: %(default)s)')
    args = parser.parse_args()

    # Run benchmarks
    run_benchmarks(
        args.iterations,
        args.iterations if args.events is None else args.events,
        args.step,
        args.seed,
        args.precision
    )


if __name__ == '__main__':
    main()
