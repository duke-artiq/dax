import dataclasses
import typing
import numpy as np

from artiq.coredevice.exceptions import DMAError

from artiq.language.core import *
from artiq.language.types import *
from artiq.language.units import *

from dax.sim.device import DaxSimDevice
from dax.sim.signal import get_signal_manager, Signal


@dataclasses.dataclass(frozen=True)
class _DMATrace:
    """Dataclass for storing properties of a DMA trace."""
    name: str
    epoch: int
    duration: np.int64


class DMARecordContextManager:
    """DMA recording class, which is used as a context handler but also stores all trace and handle information."""

    core: typing.Any
    name: str
    epoch: int
    saved_now_mu: np.int64
    _dma_traces: typing.Dict[str, _DMATrace]
    _dma_handles: typing.Dict[typing.Tuple[np.int32, np.int64, np.int32], str]
    _dma_record: Signal
    _dma_play: Signal
    _dma_play_name: Signal

    def __init__(self, device: DaxSimDevice):
        # Store core attribute
        self.core = device.core

        # Initialize attributes
        self.name = ""
        self.epoch = 0
        self.saved_now_mu = np.int64(0)
        self._dma_traces = {}
        self._dma_handles = {}

        # Register signal using CoreDMA object
        signal_manager = get_signal_manager()
        self._dma_record = signal_manager.register(device, 'record', str)
        self._dma_play = signal_manager.register(device, 'play', object)
        self._dma_play_name = signal_manager.register(device, 'play_name', str)

    @rpc
    def _enter(self) -> TNone:
        # Set record signal
        self._dma_record.push(self.name)

    @kernel
    def __enter__(self):  # type: () -> None
        # Save current time
        self.saved_now_mu = now_mu()
        # Call RPC
        self._enter()

    @rpc
    def _exit(self) -> TNone:
        # Reset record signal
        self._dma_record.push('')
        # Store properties of this trace
        self._dma_traces[self.name] = _DMATrace(
            name=self.name,
            epoch=self.epoch,
            duration=now_mu() - self.saved_now_mu,
        )

    @kernel
    def __exit__(self, type_, value, traceback):  # type: (typing.Any, typing.Any, typing.Any) -> None
        # Call RPC
        self._exit()

    @rpc
    def erase(self, name: TStr) -> TNone:
        """Remove DMA trace."""
        assert isinstance(name, str), 'DMA trace name must be of type str'
        self._dma_traces.pop(name, None)  # No error when name does not exist

    @host_only
    def _playback(self, trace: _DMATrace) -> None:
        assert isinstance(trace, _DMATrace), 'DMA trace has an incorrect type'

        # Place events for DMA playback
        self._dma_play.push(True)  # Represents the event of playing a trace
        self._dma_play_name.push(trace.name)  # Represents the duration of the event

        # Forward time by the duration of the DMA trace
        delay_mu(trace.duration)

        # Signal ending of DMA trace
        self._dma_play_name.push('')

    @rpc
    def playback(self, name: TStr) -> TNone:
        """Playback DMA trace."""
        assert isinstance(name, str), 'DMA trace name must be of type str'

        # Verify trace
        if name not in self._dma_traces:
            raise KeyError(f'DMA trace "{name}" does not exist, cannot play')

        # Playback trace
        self._playback(self._dma_traces[name])

    @rpc
    def get_handle(self, name: TStr) -> TTuple([TInt32, TInt64, TInt32]):  # type: ignore[valid-type]
        assert isinstance(name, str), 'DMA trace name must be of type str'

        if name not in self._dma_traces:
            raise KeyError(f'DMA trace "{name}" does not exist, cannot get handle')

        # Obtain the DMA trace
        trace = self._dma_traces[name]

        # Create an artificial but unique handle that contains the epoch and an identifier of the DMA trace
        handle = np.int32(self.epoch), np.int64(id(trace)), np.int32(0)
        # Store a mapping from this handle to the name
        self._dma_handles[handle] = name

        # Return the handle
        return handle

    @rpc
    def playback_handle(self, handle: TTuple([TInt32, TInt64, TInt32])) -> TNone:  # type: ignore[valid-type]
        # Unpack handle
        epoch, _, _ = handle

        # Verify handle
        if self.epoch != epoch:
            # An epoch mismatch occurs when adding or erasing a DMA trace after obtaining the handle
            raise DMAError('Invalid DMA handle, epoch mismatch')

        # Playback trace (if the epoch matches, the trace will exist)
        self._playback(self._dma_traces[self._dma_handles[handle]])


class CoreDMA(DaxSimDevice):

    recorder: DMARecordContextManager

    def __init__(self, dmgr: typing.Any, **kwargs: typing.Any):
        # Call super
        super(CoreDMA, self).__init__(dmgr, **kwargs)

        # Initialize recorder
        self.recorder = DMARecordContextManager(self)

    @property
    def epoch(self) -> int:
        return self.recorder.epoch

    @epoch.setter
    def epoch(self, epoch: int) -> None:
        assert isinstance(epoch, int)
        self.recorder.epoch = epoch

    @kernel
    def record(self, name):  # type: (str) -> DMARecordContextManager
        self.epoch += 1
        self.recorder.name = name
        return self.recorder

    @kernel
    def erase(self, name: TStr) -> TNone:
        self.epoch += 1
        self.recorder.erase(name)

    @kernel
    def playback(self, name: TStr) -> TNone:
        self.recorder.playback(name)

    @kernel
    def get_handle(self, name: TStr) -> TTuple([TInt32, TInt64, TInt32]):  # type: ignore[valid-type]
        return self.recorder.get_handle(name)

    @kernel
    def playback_handle(self, handle: TTuple([TInt32, TInt64, TInt32])) -> TNone:  # type: ignore[valid-type]
        self.recorder.playback_handle(handle)
