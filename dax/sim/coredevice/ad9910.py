# mypy: disallow_untyped_defs = False
# mypy: disallow_incomplete_defs = False
# mypy: check_untyped_defs = False

import dataclasses
import typing
import warnings

from numpy import int32, int64

from artiq.language.core import kernel, delay, portable, now_mu, at_mu, delay_mu
from artiq.language.units import us, ms
from artiq.language.types import TBool, TInt32, TInt64, TFloat, TList, TTuple
from artiq.coredevice.ad9910 import (
    PHASE_MODE_CONTINUOUS, PHASE_MODE_ABSOLUTE, PHASE_MODE_TRACKING,
    RAM_DEST_FTW, RAM_DEST_POW, RAM_DEST_ASF, RAM_DEST_POWASF,
    urukul_sta_pll_lock
)

from dax.util.artiq_version import ARTIQ_MAJOR_VERSION
from dax.sim.device import DaxSimDevice
from dax.sim.signal import get_signal_manager
from dax.sim.coredevice.urukul import CPLD, DEFAULT_PROFILE, NUM_PROFILES

_DEFAULT_PROFILE_RAM = 0

_PHASE_MODE_DEFAULT = -1
_PHASE_MODE_DICT = {m: f'{m:02b}' for m in [PHASE_MODE_CONTINUOUS, PHASE_MODE_ABSOLUTE, PHASE_MODE_TRACKING]}
"""Phase mode conversion dict."""

_RAM_DEST_DICT = {m: f'{m:02b}' for m in [RAM_DEST_FTW, RAM_DEST_POW, RAM_DEST_ASF, RAM_DEST_POWASF]}
"""RAM destination conversion dict."""

_ADDR_TO_PROFILE = {
    0x0E: 0,
    0x0F: 1,
    0x10: 2,
    0x11: 3,
    0x12: 4,
    0x13: 5,
    0x14: 6,
    0x15: 7,
}
"""Dict to convert an address to a profile."""


@dataclasses.dataclass
class _SingleToneProfile:
    freq: float = 0.0
    phase: float = 0.0
    amp: float = 0.0

    def write(self, freq, phase, amp) -> None:
        self.freq = freq
        self.phase = phase
        self.amp = amp

    def read(self) -> TTuple([TFloat, TFloat, TFloat]):  # type: ignore[valid-type]
        return self.freq, self.phase, self.amp

    def clear(self) -> None:
        self.freq = 0.0
        self.phase = 0.0
        self.amp = 0.0


@dataclasses.dataclass
class _AD9910Registers:
    power_down: int
    ram_enable: int
    ram_dest: int
    phase_autoclear: int
    internal_profile: int
    ram_tone: _SingleToneProfile
    single_tone_profiles: typing.List[_SingleToneProfile]


class SyncDataUser:
    def __init__(self, core, sync_delay_seed, io_update_delay):
        self.core = core
        self.sync_delay_seed = sync_delay_seed
        self.io_update_delay = io_update_delay

    @kernel
    def init(self):
        pass


class SyncDataEeprom:
    def __init__(self, dmgr, core, eeprom_str):
        self.core = core
        self.sync_delay_seed = 0
        self.io_update_delay = 0

    @kernel
    def init(self):
        self.sync_delay_seed = -1
        self.io_update_delay = 0


class AD9910(DaxSimDevice):

    cpld: CPLD
    _profile: int
    _reg: _AD9910Registers

    def __init__(self, dmgr, chip_select, cpld_device, sw_device=None,
                 pll_n=40, pll_cp=7, pll_vco=5, sync_delay_seed=-1,
                 io_update_delay=0, pll_en=1, **kwargs):
        # Call super
        super(AD9910, self).__init__(dmgr, **kwargs)

        # CPLD device
        self.cpld = dmgr.get(cpld_device)
        self.cpld.io_update.pulse_subscribe(self._io_update)
        self.cpld.profile_subscribe(self._profile_update)
        # Bus device
        self.bus = self.cpld.bus
        # Chip select
        assert 3 <= chip_select <= 7
        self.chip_select = chip_select
        # Switch device
        if sw_device:
            self.sw = dmgr.get(sw_device)

        # Store attributes (from ARTIQ code)
        clk = self.cpld.refclk / [4, 1, 2, 4][self.cpld.clk_div]
        self.pll_en = pll_en
        self.pll_n = pll_n
        self.pll_vco = pll_vco
        self.pll_cp = pll_cp
        if pll_en:
            sysclk = clk * pll_n
            assert clk <= 60e6
            assert 12 <= pll_n <= 127
            assert 0 <= pll_vco <= 5
            vco_min, vco_max = [(370, 510), (420, 590), (500, 700),
                                (600, 880), (700, 950), (820, 1150)][pll_vco]
            assert vco_min <= sysclk / 1e6 <= vco_max
            assert 0 <= pll_cp <= 7
        else:
            sysclk = clk
        assert sysclk <= 1e9
        self.ftw_per_hz: float = (1 << 32) / sysclk
        self.sysclk_per_mu = int(round(float(sysclk * self.core.ref_period)))
        self.sysclk = sysclk

        if isinstance(sync_delay_seed, str) or isinstance(io_update_delay,
                                                          str):
            if sync_delay_seed != io_update_delay:
                raise ValueError("When using EEPROM, sync_delay_seed must be "
                                 "equal to io_update_delay")
            self.sync_data = SyncDataEeprom(dmgr, self.core, sync_delay_seed)
        else:
            self.sync_data = SyncDataUser(self.core, sync_delay_seed,
                                          io_update_delay)

        self.phase_mode = PHASE_MODE_CONTINUOUS

        # Register signals
        signal_manager = get_signal_manager()
        self._init = signal_manager.register(self, 'init', bool, size=1)
        self._freq = signal_manager.register(self, 'freq', float)
        self._phase = signal_manager.register(self, 'phase', float)  # Phase in turns
        self._phase_mode = signal_manager.register(self, 'phase_mode', bool, size=2)
        self._phase_clear = signal_manager.register(self, 'phase_clear', object)
        self._amp = signal_manager.register(self, 'amp', float)
        self._ram_enable = signal_manager.register(self, 'ram_enable', bool, size=1)
        self._ram_dest = signal_manager.register(self, 'ram_dest', bool, size=2)
        self._internal_profile = signal_manager.register(self, 'internal_profile', int)
        self._power_down = signal_manager.register(self, 'power_down', bool, size=4)

        # Internal registers
        self._profile = DEFAULT_PROFILE
        self._reg = _AD9910Registers(0, 0, RAM_DEST_FTW, 0, 0, _SingleToneProfile(),
                                     [_SingleToneProfile() for _ in range(NUM_PROFILES)])

    def _io_update(self):
        # Update cfg signals
        self._power_down.push(f'{self._reg.power_down & 0b1111:04b}')
        self._ram_enable.push(self._reg.ram_enable)
        self._ram_dest.push(_RAM_DEST_DICT[self._reg.ram_dest])
        self._internal_profile.push(self._reg.internal_profile)
        if self._reg.phase_autoclear:
            self._phase_clear.push(True)

        # Obtain values based on mode
        if self._reg.ram_enable:
            freq = self._reg.ram_tone.freq if self._reg.ram_dest != RAM_DEST_FTW else 0.0
            phase = self._reg.ram_tone.phase if self._reg.ram_dest not in {RAM_DEST_POW, RAM_DEST_POWASF} else 0.0
            amp = self._reg.ram_tone.amp if self._reg.ram_dest not in {RAM_DEST_ASF, RAM_DEST_POWASF} else 0.0
        else:
            profile = self._reg.single_tone_profiles[self._profile]
            freq = profile.freq
            phase = profile.phase
            amp = profile.amp

        # Update signals for freq, phase, and amp
        self._freq.push(freq)
        self._phase.push(phase)
        self._amp.push(amp)

    def _profile_update(self, profile):
        self._profile = profile
        self._io_update()

    @kernel
    def set_phase_mode(self, phase_mode: TInt32):
        # From ARTIQ code
        self.phase_mode = phase_mode

    @kernel
    def write16(self, addr: TInt32, data: TInt32):
        raise NotImplementedError

    @kernel
    def write32(self, addr: TInt32, data: TInt32):
        raise NotImplementedError

    @kernel
    def read16(self, addr: TInt32) -> TInt32:
        raise NotImplementedError

    @kernel
    def read32(self, addr: TInt32) -> TInt32:
        raise NotImplementedError

    @kernel
    def read64(self, addr: TInt32) -> TInt64:
        raise NotImplementedError

    def _write64(self, addr: TInt32, data_high: TInt32, data_low: TInt32):
        """Support ``write64()`` for single-tone profiles.

        This is based on the assumption that writes to profile registers are actually single-tone profile writes and
        not RAM profile writes. We think that is a safe assumption because RAM profile writes are already well isolated
        in :func:`set_profile_ram` while single-tone profile writes are embedded in :func:`set_mu` which users might
        want to customize.
        """
        if addr in _ADDR_TO_PROFILE:
            # Convert values to SI units
            frequency = self.ftw_to_frequency(int32(data_low))
            phase = self.pow_to_turns(int32(data_high) & 0xFFFF)
            amplitude = self.asf_to_amplitude((int32(data_high) >> 16) & 0x3FFF)
            # Write registers
            self._reg.single_tone_profiles[_ADDR_TO_PROFILE[addr]].write(frequency, phase, amplitude)
        else:
            raise NotImplementedError

    @kernel
    def write64(self, addr: TInt32, data_high: TInt32, data_low: TInt32):
        self._write64(addr, data_high, data_low)

    @kernel
    def write_ram(self, data: TList(TInt32)):  # type: ignore[valid-type]
        # Simplified RAM mode simulation
        assert len(data) <= 1024, 'Data exceeds size of RAM'

    @kernel
    def read_ram(self, data: TList(TInt32)):  # type: ignore[valid-type]
        raise NotImplementedError

    def _set_cfr1(self,
                  power_down: TInt32,
                  phase_autoclear: TInt32,
                  drg_load_lrr: TInt32, drg_autoclear: TInt32,
                  phase_clear: TInt32,
                  internal_profile: TInt32,
                  ram_destination: TInt32, ram_enable: TInt32,
                  manual_osk_external: TInt32, osk_enable: TInt32, select_auto_osk: TInt32):
        assert drg_load_lrr == drg_autoclear == 0, 'DRG currently not supported in simulation'
        assert manual_osk_external == osk_enable == select_auto_osk == 0, 'OSK currently not supported in simulation'
        assert 0 <= internal_profile < NUM_PROFILES, 'Internal profile out of range'

        # Store configuration in registers
        self._reg.power_down = power_down
        self._reg.ram_enable = ram_enable & 0x1
        self._reg.ram_dest = ram_destination & 0x3
        self._reg.phase_autoclear = phase_autoclear & 0x1
        self._reg.internal_profile = internal_profile & 0x7

        # Write power down bits that do not need IO update
        self._power_down.push(f'{power_down & 0b1010:04b}')

        if phase_clear & 0x1:
            # Handle async phase clear
            self._phase_clear.push(True)

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def set_cfr1(self,
                     power_down: TInt32 = 0b0000,
                     phase_autoclear: TInt32 = 0,
                     drg_load_lrr: TInt32 = 0, drg_autoclear: TInt32 = 0,
                     phase_clear: TInt32 = 0,
                     internal_profile: TInt32 = 0,
                     ram_destination: TInt32 = 0, ram_enable: TInt32 = 0,
                     manual_osk_external: TInt32 = 0, osk_enable: TInt32 = 0, select_auto_osk: TInt32 = 0):
            self._set_cfr1(
                power_down=power_down,
                phase_autoclear=phase_autoclear,
                drg_load_lrr=drg_load_lrr,
                drg_autoclear=drg_autoclear,
                phase_clear=phase_clear,
                internal_profile=internal_profile,
                ram_destination=ram_destination,
                ram_enable=ram_enable,
                manual_osk_external=manual_osk_external,
                osk_enable=osk_enable,
                select_auto_osk=select_auto_osk
            )

    elif ARTIQ_MAJOR_VERSION == 6:  # pragma: no cover
        @kernel
        def set_cfr1(self,
                     power_down: TInt32 = 0b0000,
                     phase_autoclear: TInt32 = 0,
                     drg_load_lrr: TInt32 = 0, drg_autoclear: TInt32 = 0,
                     internal_profile: TInt32 = 0,
                     ram_destination: TInt32 = 0, ram_enable: TInt32 = 0,
                     manual_osk_external: TInt32 = 0, osk_enable: TInt32 = 0, select_auto_osk: TInt32 = 0):
            self._set_cfr1(
                power_down=power_down,
                phase_autoclear=phase_autoclear,
                drg_load_lrr=drg_load_lrr,
                drg_autoclear=drg_autoclear,
                phase_clear=0,
                internal_profile=internal_profile,
                ram_destination=ram_destination,
                ram_enable=ram_enable,
                manual_osk_external=manual_osk_external,
                osk_enable=osk_enable,
                select_auto_osk=select_auto_osk
            )

    else:  # pragma: no cover
        # ARTIQ 5
        @kernel
        def set_cfr1(self,
                     power_down=0b0000,
                     phase_autoclear=0,
                     drg_load_lrr=0, drg_autoclear=0,
                     internal_profile=0,
                     ram_destination=0, ram_enable=0
                     ):
            self._set_cfr1(
                power_down=power_down,
                phase_autoclear=phase_autoclear,
                drg_load_lrr=drg_load_lrr,
                drg_autoclear=drg_autoclear,
                phase_clear=0,
                internal_profile=internal_profile,
                ram_destination=ram_destination,
                ram_enable=ram_enable,
                manual_osk_external=0,
                osk_enable=0,
                select_auto_osk=0
            )

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def set_cfr2(self,
                     asf_profile_enable: TInt32 = 1,
                     drg_enable: TInt32 = 0,
                     effective_ftw: TInt32 = 1,
                     sync_validation_disable: TInt32 = 0,
                     matched_latency_enable: TInt32 = 0):
            raise NotImplementedError

    @kernel
    def init(self, blind: TBool = False):
        self.sync_data.init()
        if self.sync_data.sync_delay_seed >= 0 and not self.cpld.sync_div:
            raise ValueError("parent cpld does not drive SYNC")
        if self.sync_data.sync_delay_seed >= 0:
            if self.sysclk_per_mu != self.sysclk * self.core.ref_period:
                raise ValueError("incorrect clock ratio for synchronization")
        delay(50 * ms)  # slack

        # Set SPI mode
        self.set_cfr1()
        self.cpld.io_update.pulse(1 * us)
        delay(1 * ms)
        if not blind:
            delay(50 * us)  # slack
        # Configure PLL settings and bring up PLL
        # enable amplitude scale from profiles
        # read effective FTW
        # sync timing validation disable (enabled later)
        pass
        self.cpld.io_update.pulse(1 * us)
        pass
        pass  # PFD reset
        self.cpld.io_update.pulse(1 * us)
        if self.pll_en:
            pass
            self.cpld.io_update.pulse(1 * us)
            if blind:
                delay(100 * ms)
            else:
                # Wait for PLL lock, up to 100 ms
                for i in range(100):
                    sta = self.cpld.sta_read()
                    lock = urukul_sta_pll_lock(sta)
                    delay(1 * ms)
                    if lock & (1 << self.chip_select - 4):
                        break
                    if i >= 100 - 1:
                        raise ValueError("PLL lock timeout")
        delay(10 * us)  # slack
        if self.sync_data.sync_delay_seed >= 0 and not blind:
            self.tune_sync_delay(self.sync_data.sync_delay_seed)
        delay(1 * ms)

        # Update signal
        self._init.push(True)

    @kernel
    def power_down(self, bits: TInt32 = 0b1111):
        self.set_cfr1(power_down=bits)
        self.cpld.io_update.pulse(1 * us)

    def _set_mu(self, ftw: TInt32, pow_: TInt32, asf: TInt32,
                phase_mode: TInt32,
                ref_time_mu: TInt64, profile: TInt32,
                ram_destination: TInt32) -> TInt32:
        assert 0 <= profile < NUM_PROFILES, 'Profile out of range'
        assert ram_destination in {-1, RAM_DEST_FTW, RAM_DEST_POW, RAM_DEST_ASF, RAM_DEST_POWASF}, \
            'Invalid RAM destination'

        # From ARTIQ
        if phase_mode == _PHASE_MODE_DEFAULT:
            phase_mode = self.phase_mode
        # Align to coarse RTIO which aligns SYNC_CLK. I.e. clear fine TSC
        # This will not cause a collision or sequence error.
        at_mu(now_mu() & ~7)
        if phase_mode != PHASE_MODE_CONTINUOUS:
            # Auto-clear phase accumulator on IO_UPDATE.
            # This is active already for the next IO_UPDATE
            self.set_cfr1(phase_autoclear=1)
            if phase_mode == PHASE_MODE_TRACKING and ref_time_mu < 0:
                # set default fiducial time stamp
                ref_time_mu = 0
            if ref_time_mu >= 0:
                # 32 LSB are sufficient.
                # Also no need to use IO_UPDATE time as this
                # is equivalent to an output pipeline latency.
                dt = int32(now_mu()) - int32(ref_time_mu)
                # Suppress numpy warning about overflowing int32
                with warnings.catch_warnings():
                    warnings.simplefilter('ignore', category=RuntimeWarning)
                    pow_ += dt * ftw * self.sysclk_per_mu >> 16
        if ram_destination == -1:
            # Convert values to SI units
            frequency = self.ftw_to_frequency(int32(ftw))
            phase = self.pow_to_turns(int32(pow_) & 0xFFFF)
            amplitude = self.asf_to_amplitude(int32(asf) & 0x3FFF)
            # Write registers
            self._reg.single_tone_profiles[profile].write(frequency, phase, amplitude)
        else:
            if not ram_destination == RAM_DEST_FTW:
                self.set_ftw(ftw)
            if not ram_destination == RAM_DEST_POWASF:
                if not ram_destination == RAM_DEST_ASF:
                    self.set_asf(asf)
                if not ram_destination == RAM_DEST_POW:
                    self.set_pow(pow_)
        delay_mu(int64(self.sync_data.io_update_delay))
        self.cpld.io_update.pulse_mu(8)  # assumes 8 mu > t_SYN_CCLK
        self._phase_mode.push(_PHASE_MODE_DICT[phase_mode])
        at_mu(now_mu() & ~7)  # clear fine TSC again
        if phase_mode != PHASE_MODE_CONTINUOUS:
            self.set_cfr1()
            # future IO_UPDATE will activate
        return pow_

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def set_mu(self, ftw: TInt32 = 0, pow_: TInt32 = 0, asf: TInt32 = 0x3fff,
                   phase_mode: TInt32 = _PHASE_MODE_DEFAULT,
                   ref_time_mu: TInt64 = int64(-1), profile: TInt32 = DEFAULT_PROFILE,
                   ram_destination: TInt32 = -1) -> TInt32:
            return self._set_mu(ftw=ftw, pow_=pow_, asf=asf,
                                phase_mode=phase_mode,
                                ref_time_mu=ref_time_mu, profile=profile,
                                ram_destination=ram_destination)

    else:  # pragma: no cover
        @kernel
        def set_mu(self, ftw: TInt32, pow_: TInt32 = 0, asf: TInt32 = 0x3fff,
                   phase_mode: TInt32 = _PHASE_MODE_DEFAULT,
                   ref_time_mu: TInt64 = int64(-1), profile: TInt32 = DEFAULT_PROFILE) -> TInt32:
            return self._set_mu(ftw=ftw, pow_=pow_, asf=asf,
                                phase_mode=phase_mode,
                                ref_time_mu=ref_time_mu, profile=profile,
                                ram_destination=-1)

    # noinspection PyUnusedLocal
    @kernel
    def set_profile_ram(self, start: TInt32, end: TInt32, step: TInt32 = 1,
                        profile: TInt32 = _DEFAULT_PROFILE_RAM, nodwell_high: TInt32 = 0,
                        zero_crossing: TInt32 = 0, mode: TInt32 = 1):
        # Simplified RAM mode simulation
        assert 0 <= profile < NUM_PROFILES, 'Profile out of range'
        # Single tone profile is overwritten, clear the internal registers to indicate that
        self._reg.single_tone_profiles[profile].clear()

    @kernel
    def set_ftw(self, ftw: TInt32):
        frequency = self.ftw_to_frequency(int32(ftw))
        self._reg.ram_tone.freq = frequency

    @kernel
    def set_asf(self, asf: TInt32):
        amplitude = self.asf_to_amplitude(int32(asf) & 0x3FFF)
        self._reg.ram_tone.amp = amplitude

    @kernel
    def set_pow(self, pow_: TInt32):
        phase = self.pow_to_turns(int32(pow_) & 0xFFFF)
        self._reg.ram_tone.phase = phase

    @portable(flags={"fast-math"})
    def frequency_to_ftw(self, frequency: TFloat) -> TInt32:
        return int32(round(float(self.ftw_per_hz * frequency)))

    @portable(flags={"fast-math"})
    def ftw_to_frequency(self, ftw: TInt32) -> TFloat:
        return float(ftw / self.ftw_per_hz)

    @portable(flags={"fast-math"})
    def turns_to_pow(self, turns: TFloat) -> TInt32:
        return int32(round(float(turns * 0x10000))) & int32(0xffff)

    @portable(flags={"fast-math"})
    def pow_to_turns(self, pow_: TInt32) -> TFloat:
        return float(pow_ / 0x10000)

    @portable(flags={"fast-math"})
    def amplitude_to_asf(self, amplitude: TFloat) -> TInt32:
        code = int32(round(float(amplitude * 0x3fff)))
        if code < 0 or code > 0x3fff:
            raise ValueError("Invalid AD9910 fractional amplitude!")
        return code

    @portable(flags={"fast-math"})
    def asf_to_amplitude(self, asf: TInt32) -> TFloat:
        return float(asf / float(0x3fff))

    @portable(flags={"fast-math"})
    def frequency_to_ram(self, frequency: TList(TFloat), ram: TList(TInt32)):  # type: ignore[valid-type]
        for i in range(len(ram)):
            ram[i] = self.frequency_to_ftw(frequency[i])

    @portable(flags={"fast-math"})
    def turns_to_ram(self, turns: TList(TFloat), ram: TList(TInt32)):  # type: ignore[valid-type]
        for i in range(len(ram)):
            ram[i] = self.turns_to_pow(turns[i]) << 16

    @portable(flags={"fast-math"})
    def amplitude_to_ram(self, amplitude: TList(TFloat), ram: TList(TInt32)):  # type: ignore[valid-type]
        for i in range(len(ram)):
            ram[i] = self.amplitude_to_asf(amplitude[i]) << 18

    @portable(flags={"fast-math"})
    def turns_amplitude_to_ram(self, turns: TList(TFloat),  # type: ignore[valid-type]
                               amplitude: TList(TFloat), ram: TList(TInt32)):  # type: ignore[valid-type]
        for i in range(len(ram)):
            ram[i] = ((self.turns_to_pow(turns[i]) << 16) | self.amplitude_to_asf(amplitude[i]) << 2)

    @kernel
    def set_frequency(self, frequency: TFloat):
        self.set_ftw(self.frequency_to_ftw(frequency))

    @kernel
    def set_amplitude(self, amplitude: TFloat):
        self.set_asf(self.amplitude_to_asf(amplitude))

    @kernel
    def set_phase(self, turns: TFloat):
        self.set_pow(self.turns_to_pow(turns))

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def set(self, frequency: TFloat = 0.0, phase: TFloat = 0.0,
                amplitude: TFloat = 1.0, phase_mode: TInt32 = _PHASE_MODE_DEFAULT,
                ref_time_mu: TInt64 = int64(-1), profile: TInt32 = DEFAULT_PROFILE,
                ram_destination: TInt32 = -1) -> TFloat:
            return self.pow_to_turns(self.set_mu(
                self.frequency_to_ftw(frequency), self.turns_to_pow(phase),
                self.amplitude_to_asf(amplitude), phase_mode, ref_time_mu,
                profile, ram_destination))

    else:  # pragma: no cover
        @kernel
        def set(self, frequency: TFloat, phase: TFloat = 0.0,
                amplitude: TFloat = 1.0, phase_mode: TInt32 = _PHASE_MODE_DEFAULT,
                ref_time_mu: TInt64 = int64(-1), profile: TInt32 = DEFAULT_PROFILE) -> TFloat:
            return self.pow_to_turns(self.set_mu(
                self.frequency_to_ftw(frequency), self.turns_to_pow(phase),
                self.amplitude_to_asf(amplitude), phase_mode, ref_time_mu,
                profile))

    @kernel
    def set_att_mu(self, att: TInt32):
        self.cpld.set_att_mu(self.chip_select - 4, att)

    @kernel
    def set_att(self, att: TFloat):
        self.cpld.set_att(self.chip_select - 4, att)

    @kernel
    def cfg_sw(self, state: TBool):
        self.cpld.cfg_sw(self.chip_select - 4, state)

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def set_sync(self,
                     in_delay: TInt32,
                     window: TInt32,
                     en_sync_gen: TInt32 = 0):
            pass

    else:  # pragma: no cover
        @kernel
        def set_sync(self, in_delay: TInt32, window: TInt32):
            pass

    @kernel
    def clear_smp_err(self):
        self.cpld.io_update.pulse(1 * us)
        delay(10 * us)  # slack
        self.cpld.io_update.pulse(1 * us)

    @kernel
    def tune_sync_delay(self, search_seed: TInt32 = 15) -> TTuple([TInt32, TInt32]):  # type: ignore[valid-type]
        return search_seed, 0x0b

    @kernel
    def measure_io_update_alignment(self, delay_start: TInt64,
                                    delay_stop: TInt64) -> TInt32:
        raise NotImplementedError

    @kernel
    def tune_io_update_delay(self) -> TInt32:
        return 0

    if ARTIQ_MAJOR_VERSION >= 7:
        @kernel
        def get_ftw(self) -> TInt32:
            return self.frequency_to_ftw(self.get_frequency())

        @kernel
        def get_asf(self) -> TInt32:
            return self.amplitude_to_asf(self.get_amplitude())

        @kernel
        def get_pow(self) -> TInt32:
            return self.turns_to_pow(self.get_phase())

        @kernel
        def get_frequency(self) -> TFloat:
            return self._reg.ram_tone.freq

        @kernel
        def get_amplitude(self) -> TFloat:
            return self._reg.ram_tone.amp

        @kernel
        def get_phase(self) -> TFloat:
            return self._reg.ram_tone.phase

        @kernel
        def get_mu(self, profile: TInt32 = DEFAULT_PROFILE) \
                -> TTuple([TInt32, TInt32, TInt32]):  # type: ignore[valid-type]
            freq, phase, amp = self.get(profile)
            return self.frequency_to_ftw(freq), self.turns_to_pow(phase), self.amplitude_to_asf(amp)

        @kernel
        def get(self, profile: TInt32 = DEFAULT_PROFILE) \
                -> TTuple([TFloat, TFloat, TFloat]):  # type: ignore[valid-type]
            assert 0 <= profile < NUM_PROFILES, 'Profile out of range'
            return self._reg.single_tone_profiles[profile].read()

        @kernel
        def get_att_mu(self) -> TInt32:
            return self.cpld.get_channel_att_mu(self.chip_select - 4)

        @kernel
        def get_att(self) -> TFloat:
            return self.cpld.get_channel_att(self.chip_select - 4)
