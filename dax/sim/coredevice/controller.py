"""This module contains simulation classes that match drivers in ``dax.util.controller``."""

import logging
from typing import Any, Dict, Sequence

from dax.sim.device import DaxSimDevice
import dax.util.controller

__all__ = ["LocalControllerAdapter", "SubprocessControllerAdapter"]

_logger: logging.Logger = logging.getLogger(__name__)
"""The logger for this file."""

_SIMULATION_ARGS: Sequence[str] = ['--simulation', '--no-localhost-bind']
"""The simulation options/arguments to add to controllers."""


class LocalControllerAdapter(dax.util.controller.LocalControllerAdapter, DaxSimDevice):
    """Simulation class for :class:`dax.util.controller.LocalControllerAdapter`."""

    def __init__(
        self,
        dmgr: Any,
        *args: Any,
        _key: str,
        _core: Any = None,
        core_device: str = 'core',
        arguments: Dict[str, Any],
        **kwargs: Any,
    ):
        # Initialize the DaxSimDevice superclass manually
        DaxSimDevice.__init__(self, dmgr, _key=_key, _core=_core, core_device=core_device)

        # Add simulation flag to arguments (assume this flag is available in driver)
        _logger.debug(f'local controller adapter "{_key}": adding simulation flag to arguments')
        arguments["simulation"] = True

        # Call super to initialize the controller adapter class
        super().__init__(dmgr, *args, arguments=arguments, **kwargs)  # type: ignore[misc]


class SubprocessControllerAdapter(dax.util.controller.SubprocessControllerAdapter, DaxSimDevice):
    """Simulation class for :class:`dax.util.controller.SubprocessControllerAdapter`."""

    def __init__(
        self,
        dmgr: Any,
        *args: Any,
        _key: str,
        _core: Any = None,
        core_device: str = 'core',
        command: str,
        **kwargs: Any,
    ):
        # Initialize the DaxSimDevice superclass manually
        DaxSimDevice.__init__(self, dmgr, _key=_key, _core=_core, core_device=core_device)

        # Add simulation arguments
        sim_args = [a for a in _SIMULATION_ARGS if a not in command]
        if sim_args:
            # Add simulation arguments
            sim_args_join = " ".join(sim_args)
            command = f"{command} {sim_args_join}"
            _logger.debug(
                f'subprocess controller adapter "{_key}": added simulation argument(s) "{sim_args_join}" to command'
            )
        else:
            # No simulation arguments added
            _logger.debug(f'subprocess controller adapter "{_key}": command not modified')

        # Call super to initialize the controller adapter class
        super().__init__(dmgr, *args, command=command, **kwargs)  # type: ignore[misc]
