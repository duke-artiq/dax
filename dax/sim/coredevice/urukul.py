# mypy: disallow_untyped_defs = False
# mypy: disallow_incomplete_defs = False
# mypy: check_untyped_defs = False

import typing
import numpy as np
import warnings

from artiq.language.core import kernel, portable, host_only, delay, delay_mu, at_mu, now_mu
from artiq.language.units import us, ms, dB
from artiq.language.types import TInt32, TFloat, TBool
from artiq.coredevice.urukul import (
    urukul_cfg, urukul_sta_proto_rev,
    CFG_RST, CFG_IO_RST, CFG_RF_SW, CFG_PROFILE, STA_PROTO_REV, STA_PROTO_REV_MATCH, STA_PLL_LOCK,
    _DummySync,
)

from dax.util.artiq_version import ARTIQ_MAJOR_VERSION
from dax.sim.device import DaxSimDevice
from dax.sim.signal import get_signal_manager

DEFAULT_PROFILE = 0 if ARTIQ_MAJOR_VERSION < 7 else 7
NUM_PROFILES = 8

_NUM_CHANNELS = 4


@portable(flags={'fast-math'})
def _mu_to_att(att_mu: TInt32) -> TFloat:
    return float((255 - (att_mu & 0xFF)) / 8)


@portable(flags={'fast-math'})
def _att_to_mu(att: TFloat) -> TInt32:
    code = 255 - np.int32(round(att * 8))
    if code < 0 or code > 255:
        raise ValueError("Invalid urukul.CPLD attenuation!")
    return code


def _state_to_sw_reg(state: typing.Union[int, np.int32]) -> typing.List[str]:
    return ['1' if (state >> i) & 0x1 else '0' for i in range(4)]


class _RegIOUpdate:
    _subscribers: typing.List[typing.Callable[[], typing.Any]]

    def __init__(self, cpld):
        # Store attributes
        self.cpld = cpld
        # Subscribers
        self._subscribers = []

    def pulse_mu(self, t):
        delay_mu(t)

        # Notify
        for fn in self._subscribers:
            fn()

    def pulse(self, t):
        self.pulse_mu(self.cpld.core.seconds_to_mu(t))

    @host_only
    def pulse_subscribe(self, fn: typing.Callable[[], typing.Any]) -> None:
        """Subscribe to :func:`pulse` and :func:`pulse_mu` calls of this device.

        :param fn: Callback function for the notification
        """
        self._subscribers.append(fn)


class CPLD(DaxSimDevice):

    _subscribers: typing.List[typing.Callable[[int], typing.Any]]
    att_reg: np.int32
    _att_reg: typing.List[float]
    _profile_reg: int

    def __init__(self, dmgr, spi_device,
                 io_update_device=None, dds_reset_device=None, sync_device=None,
                 sync_sel=0, clk_sel=0, clk_div=0, rf_sw=0, refclk=125e6, att=0x00000000, sync_div=None, **kwargs):
        # Call super
        super(CPLD, self).__init__(dmgr, **kwargs)

        # Subscribers
        self._subscribers = []

        # Store attributes (from ARTIQ code)
        self.refclk = refclk
        assert 0 <= clk_div <= 3
        self.clk_div = clk_div

        # Get devices (from ARTIQ code)
        self.bus = dmgr.get(spi_device)
        if io_update_device is not None:
            self.io_update = dmgr.get(io_update_device)
        else:
            self.io_update = _RegIOUpdate(self)
        if dds_reset_device is not None:
            self.dds_reset = dmgr.get(dds_reset_device)
        if sync_device is not None:
            self.sync = dmgr.get(sync_device)
            if sync_div is None:
                sync_div = 2
        else:
            self.sync = _DummySync(self)
            assert sync_div is None
            sync_div = 0

        # Store register values (from ARTIQ code)
        self.cfg_reg = urukul_cfg(rf_sw=rf_sw, led=0, profile=DEFAULT_PROFILE,
                                  io_update=0, mask_nu=0, clk_sel=clk_sel,
                                  sync_sel=sync_sel,
                                  rst=0, io_rst=0, clk_div=clk_div)
        self.att_reg = np.int32(np.int64(att))
        self.sync_div = sync_div

        # Register signals
        signal_manager = get_signal_manager()
        self._init = signal_manager.register(self, 'init', bool, size=1)
        self._init_att = signal_manager.register(self, 'init_att', bool, size=1)
        self._att = [signal_manager.register(self, f'att_{i}', float) for i in range(4)]
        self._sw = signal_manager.register(self, 'sw', bool, size=4)
        self._profile = signal_manager.register(self, 'profile', int, init='x')

        # Internal registers
        self._att_reg = [_mu_to_att(att >> (i * 8)) for i in range(4)]
        self._profile_reg = DEFAULT_PROFILE

    def _cfg_write(self, cfg: TInt32):
        # Extract switch information
        sw_reg = _state_to_sw_reg((cfg >> CFG_RF_SW) & 0xF)
        self._sw.push(''.join(reversed(sw_reg)))

        # Extract profile information
        profile = (cfg >> CFG_PROFILE) & 0xF
        self._profile.push(profile)
        if profile != self._profile_reg:
            # Notify subscribers
            self._profile_reg = profile
            self._profile_notify(profile)

    @kernel
    def cfg_write(self, cfg: TInt32):
        self._cfg_write(cfg)
        self.cfg_reg = cfg

    @kernel
    def sta_read(self) -> TInt32:
        # Only returns PROTO_REV and PLL_LOCK at this moment
        return (STA_PROTO_REV_MATCH << STA_PROTO_REV) | (0xF << STA_PLL_LOCK)

    # noinspection PyUnusedLocal
    @kernel
    def init(self, blind: TBool = False):
        cfg = self.cfg_reg
        # Don't pulse MASTER_RESET (m-labs/artiq#940)
        self.cfg_reg = cfg | (0 << CFG_RST) | (1 << CFG_IO_RST)
        if blind:
            self.cfg_write(self.cfg_reg)
        else:
            proto_rev = urukul_sta_proto_rev(self.sta_read())
            if proto_rev != STA_PROTO_REV_MATCH:
                raise ValueError("Urukul proto_rev mismatch")
        delay(100 * us)  # reset, slack
        self.cfg_write(cfg)
        if self.sync_div:
            at_mu(now_mu() & ~0xf)  # align to RTIO/2
            self.set_sync_div(self.sync_div)  # 125 MHz/2 = 1 GHz/16
        delay(1 * ms)  # DDS wake up

        self._init.push(True)

    @kernel
    def io_rst(self):
        self.cfg_write(self.cfg_reg | (1 << CFG_IO_RST))
        self.cfg_write(self.cfg_reg & ~(1 << CFG_IO_RST))

    @kernel
    def cfg_sw(self, channel: TInt32, on: TBool):
        assert 0 <= channel < _NUM_CHANNELS, 'Channel out of range'

        c = self.cfg_reg
        if on:
            c |= 1 << channel
        else:
            c &= ~(1 << channel)
        self.cfg_write(c)

    @kernel
    def cfg_switches(self, state: TInt32):
        self.cfg_write((self.cfg_reg & ~0xf) | state)

    @kernel
    def set_att_mu(self, channel: TInt32, att: TInt32):
        assert 0 <= channel < _NUM_CHANNELS, 'Channel out of range'
        assert 0 <= att <= 255, 'Attenuation mu out of range'
        a = self.att_reg & ~(0xff << (channel * 8))
        a |= att << (channel * 8)
        self.set_all_att_mu(a)

    def _set_all_att_mu(self, att_reg: TInt32):
        # Suppress numpy warning about overflowing int32 conversion.
        # See https://gitlab.com/duke-artiq/dax/-/issues/141#note_1416544123
        with warnings.catch_warnings():
            warnings.simplefilter('ignore', category=DeprecationWarning)
            self.att_reg = np.int32(att_reg)
        self._att_reg = [_mu_to_att(att_reg >> (i * 8)) for i in range(4)]
        self._update_att()

    @kernel
    def set_all_att_mu(self, att_reg: TInt32):
        self._set_all_att_mu(att_reg)

    @kernel
    def set_att(self, channel: TInt32, att: TFloat):
        assert 0 <= channel < _NUM_CHANNELS, 'Channel out of range'
        # Update register
        a = self.att_reg & ~(0xff << (channel * 8))
        a |= _att_to_mu(att) << (channel * 8)
        self.att_reg = np.int32(a)
        # Handle signals
        self._att_reg[channel] = float(att)
        self._update_att()

    def _update_att(self):  # type: () -> None
        for s, a in zip(self._att, self._att_reg):
            assert 0 * dB <= a <= (255 / 8) * dB, 'Attenuation out of range'
            s.push(a)

    @kernel
    def get_att_mu(self) -> TInt32:
        # Returns the value in the register instead of the device value
        delay(10 * us)  # Delay from ARTIQ code
        self._init_att.push(True)
        return self.att_reg

    @kernel
    def set_sync_div(self, div: TInt32):
        ftw_max = 1 << 4
        ftw = ftw_max // div
        assert ftw * div == ftw_max
        self.sync.set_mu(ftw)

    @kernel
    def set_profile(self, profile: TInt32):
        assert 0 <= profile < NUM_PROFILES, 'Profile out of range'
        cfg = self.cfg_reg & ~(7 << CFG_PROFILE)
        cfg |= (profile & 7) << CFG_PROFILE
        self.cfg_write(cfg)

    if ARTIQ_MAJOR_VERSION >= 7:
        @portable(flags={"fast-math"})
        def mu_to_att(self, att_mu: TInt32) -> TFloat:
            return _mu_to_att(att_mu)

        @portable(flags={"fast-math"})
        def att_to_mu(self, att: TFloat) -> TInt32:
            return _att_to_mu(att)

        @kernel
        def get_channel_att_mu(self, channel: TInt32) -> TInt32:
            return np.int32((self.get_att_mu() >> (channel * 8)) & 0xff)

        @kernel
        def get_channel_att(self, channel: TInt32) -> TFloat:
            return self.mu_to_att(self.get_channel_att_mu(channel))

    @host_only
    def profile_subscribe(self, fn: typing.Callable[[int], typing.Any]) -> None:
        """Subscribe to :func:`set_profile` calls of this device.

        :param fn: Callback function for the notification
        """
        self._subscribers.append(fn)

    def _profile_notify(self, profile: int) -> None:
        for fn in self._subscribers:
            fn(profile)
