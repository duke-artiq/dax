import numpy as np
import itertools
import typing
import time

from dax.experiment import *
from dax.util.units import freq_to_str, time_to_str

import artiq.coredevice.ad9910


__all__ = ["TrapRfAsfModule"]


class TrapRfAsfModule(DaxModule):
    """A trap RF module using an AD9910.

    This module controls an AD9910 used for trap RF. It can safely ramp the power of the DDS to not damage the trap.
    The module ramps linear over ASF.

    Notes when considering using this module:

    - It might not be desired to use a DDS controlled by ARTIQ. If the core device needs to be power cycled, the RF
      signal will be interrupted.
    - Any code outside this DAX module might control the DDS in an unsafe manner. This includes the startup kernel and
      the idle kernel.
    - Code outside this DAX module could modify system datasets or (class) attributes in an unsafe manner.
    """

    _FREQ_KEY: typing.ClassVar[str] = "freq"
    """Key for the frequency."""
    _RAMP_SLOPE_KEY: typing.ClassVar[str] = "ramp_slope"
    """Key for the slope of the amplitude ramp."""
    _RAMP_COMPRESSION_KEY: typing.ClassVar[str] = "ramp_compression"
    """Key for the ramp compression flag."""
    _MAX_AMP_KEY: typing.ClassVar[str] = "max_amp"
    """Key for the maximum amplitude."""

    _ENABLED_KEY: typing.ClassVar[str] = "enabled"
    """Key to store the enabled flag."""
    _LAST_AMP_KEY: typing.ClassVar[str] = "last_amp"
    """Key to store the last amplitude."""
    _LAST_FREQ_KEY: typing.ClassVar[str] = "last_freq"
    """Key to store the last frequency."""
    _CACHE_LAST_ASF_KEY: typing.ClassVar[str] = "last_asf"
    """Core device cache key to store the last ASF."""

    _MAX_AMP: typing.ClassVar[float] = 1.0
    """Constant for maximum amplitude."""
    _MIN_AMP: typing.ClassVar[float] = 0.0
    """Constant for minimum amplitude."""
    _ATT_MU: typing.ClassVar[int] = 0xFF
    """Constant attenuation value in machine units (att is fixed).
    Note that ``0xFF`` means attenuation is zero, which means that the LPF function of the attenuator is also disabled.
    Hence, the output signal has maximum amplitude but will contain harmonic noise from the DDS.
    Normally that is not an issue since the output signal often goes through an external attenuator and a trap resonator
    which both will act as filters and remove the harmonic noise. If this is an issue, please adjust accordingly.
    """
    _RAMP_STEP_PERIOD: typing.ClassVar[float] = 1.0 * ms
    """Ramp step period (i.e. delay between two ramp steps)."""
    _FACTORY_RAMP_COMPRESSION: typing.ClassVar[bool] = False
    """Default ramp compression flag value."""

    _factory_freq: float
    _factory_ramp_slope: float
    _cache_last_asf_key: str
    _trap_rf: artiq.coredevice.ad9910.AD9910
    _system: DaxSystem
    _freq: float
    _ramp_slope: float
    _ramp_compression: bool
    _max_amp: float

    def build(self, *, key: str,  # type: ignore[override]
              factory_freq: float,
              factory_ramp_slope: float) -> None:
        """Build the trap RF module.

        :param key: The key of the DDS device
        :param factory_freq: Default resonance frequency, used to calculate the allowed frequency range
        :param factory_ramp_slope: Default ramp slope in asf/second
        """
        assert isinstance(factory_freq, float)
        assert 0 < factory_freq < 400 * MHz
        assert isinstance(factory_ramp_slope, float)
        assert 0 < factory_ramp_slope

        # Check class variables
        assert self._MAX_AMP <= 1.0, "Maximum amplitude cannot be more than 1.0"
        assert self._MIN_AMP >= 0.0, "Minimum amplitude cannot be less than 0.0"
        assert self._MIN_AMP < self._MAX_AMP, "Minimum amplitude must be less than the maximum amplitude"
        assert 0x00 <= self._ATT_MU <= 0xff, "Invalid attenuation value"
        assert self._RAMP_STEP_PERIOD >= 1.0 * ms, "Ramp step period cannot be too small (it provides slack)"
        self.update_kernel_invariants("_ATT_MU", "_RAMP_STEP_PERIOD")

        # Store attributes
        self._factory_freq = factory_freq
        self._factory_ramp_slope = factory_ramp_slope
        self._cache_last_asf_key = self.get_system_key(self._CACHE_LAST_ASF_KEY)
        self.update_kernel_invariants("_cache_last_asf_key")

        # Trap RF device
        self._trap_rf = self.get_device(key, artiq.coredevice.ad9910.AD9910)
        self.update_kernel_invariants("_trap_rf")

    @host_only
    def init(self) -> None:
        """Initialize this module."""
        # Get a reference to the system
        self._system = self.registry.find_module(DaxSystem)

        # System datasets
        self._freq = self.get_dataset_sys(self._FREQ_KEY, self._factory_freq)
        self._ramp_slope = self.get_dataset_sys(self._RAMP_SLOPE_KEY, self._factory_ramp_slope)
        self._ramp_compression = self.get_dataset_sys(self._RAMP_COMPRESSION_KEY, self._FACTORY_RAMP_COMPRESSION)
        self._max_amp = self.get_dataset_sys(self._MAX_AMP_KEY, self._MAX_AMP)

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @host_only
    def ramp(self, amp: float, enabled: bool = True) -> None:
        """Set the trap RF frequency and slowly ramp the amplitude to the given value.

        :param amp: Target amplitude as amplitude scale factor (ignored if the enabled flag is :const:`False`)
        :param enabled: Enable trap RF
        :raises KeyError: Raised if no frequency was given and no previous value was available
        """
        assert isinstance(amp, float), "Amplitude must be of type float"
        assert isinstance(enabled, bool), "Enabled flag must be a bool"

        if not enabled:
            # Set the minimum amp when disabling the trap RF
            amp = self._MIN_AMP

        # Verify if the amplitude is reasonable
        if not amp <= self._MAX_AMP:
            raise ValueError(f"Amplitude {amp} is above the allowed trap RF amplitude range")
        if amp > self._max_amp:
            raise ValueError(f"Amplitude {amp} is above the protection limit")
        if amp < self._MIN_AMP:
            raise ValueError(f"Amplitude {amp} is below the allowed trap RF amplitude range")
        self.logger.debug(f"Ramp trap RF, target amplitude: {amp}")

        # Get last ASF from cache
        last_asf = self.core_cache.get(self._cache_last_asf_key)
        self.logger.debug(f"Ramp trap RF, cached last asf: {last_asf}")

        # Check if the system was just booted
        boot: bool = len(last_asf) == 0
        self.logger.debug(f"Ramp trap RF, system boot detected: {boot}")

        # Obtain last amplitude (assume lowest amplitude if none was available)
        last_amp: float = self.get_dataset_sys(self._LAST_AMP_KEY, self._MIN_AMP)
        assert isinstance(last_amp, float), "Expected type float for last amplitude"
        assert last_amp <= self._MAX_AMP, "Amplitude cannot be greater than the maximum valid amplitude"
        self.logger.debug(f"Ramp trap RF, last amplitude: {last_amp:f}")

        # Decide initial amplitude
        init_amp: float = self._MIN_AMP if boot else last_amp
        assert init_amp <= self._MAX_AMP, "Amplitude cannot be greater than the maximum valid amplitude"
        self.logger.debug(f"Ramp trap RF, initial amplitude: {init_amp:f}")

        # Decide the number of steps we need to get from the initial to the target amp
        num_steps: int = int(abs(amp - init_amp) / self._ramp_slope / self._RAMP_STEP_PERIOD)
        num_steps = max(num_steps, 2)  # Submit at least two steps
        assert num_steps >= 2, "Number of steps must be at least 2"

        # Create the steps
        amp_list = np.linspace(init_amp, amp, num=num_steps, endpoint=True, dtype=np.float64)
        assert len(amp_list) > 0, "Length of the amp list cannot be zero"
        assert amp_list[-1] == amp, "The last value in the amp list was not the target amplitude"
        assert amp_list.min() >= self._MIN_AMP, "Amplitude cannot be smaller than the minimum valid amplitude"
        assert amp_list.max() <= self._MAX_AMP, "Amplitude cannot be greater than the maximum valid amplitude"
        self.logger.debug(f"Ramp trap RF, generating ramp from {amp_list[0]:f} to {amp_list[-1]:f}")

        # Transform amplitudes to machine units (ASF)
        asf_list = np.vectorize(self._trap_rf.amplitude_to_asf)(amp_list)
        assert asf_list.min() >= 0
        assert asf_list.max() < 2 ** 14
        if self._ramp_compression:
            # Compress the list of values for efficiency, potentially speeds up ramping by removing values
            asf_list = np.asarray([k for k, _ in itertools.groupby(asf_list)], dtype=np.int32)
            assert asf_list.size > 0, "Length of the asf list cannot be zero"
        if not boot and asf_list[0] != last_asf[0]:
            raise RuntimeError("First ASF of ramp does not match last cached value, "
                               "use safety ramp down to return to a consistent state")

        # Call the kernel function
        self.logger.debug(f"Ramp trap RF, created ramp with {len(asf_list)} step(s)")
        self.logger.info(f"Ramping trap RF amplitude ({time_to_str(len(asf_list) * self._RAMP_STEP_PERIOD)})...")
        self._ramp(self._trap_rf.frequency_to_ftw(self._freq), asf_list, boot, enabled)
        self.logger.info("Trap RF amplitude ramping done")

        # Store latest values
        self.set_dataset_sys(self._ENABLED_KEY, enabled)
        self.set_dataset_sys(self._LAST_AMP_KEY, amp)
        self.set_dataset_sys(self._LAST_FREQ_KEY, self._freq)

    @kernel
    def _ramp(self, ftw: TInt32, asf_list: TArray(TInt32), boot: TBool, enabled: TBool):  # type: ignore[valid-type]
        # Reset core
        self.core.reset()

        # Make sure CPLD attenuation was loaded
        self._trap_rf.cpld.get_att_mu()
        self.core.break_realtime()

        if boot:
            # Set amplitude
            delay(1 * ms)
            self._trap_rf.set_mu(ftw, asf=0)
            # Set attenuation to desired value
            self._trap_rf.set_att_mu(self._ATT_MU)
            # Store minimum amplitude in cache
            self.core_cache.put(self._cache_last_asf_key, [0])

        # Extract cached value
        last_asf = self.core_cache.get(self._cache_last_asf_key)
        if len(last_asf) == 0:
            raise IndexError("Trap RF last ASF cache line was empty for unknown reason, aborting for safety")

        # Guarantee slack after cache operation
        self.core.break_realtime()

        if enabled:
            # Switching on BEFORE ramping
            self._trap_rf.sw.on()

        for asf in asf_list:
            # Set amplitude (in steps) and update value in cache
            delay(self._RAMP_STEP_PERIOD)
            self._trap_rf.set_mu(ftw, asf=asf)
            last_asf[0] = asf

        if not enabled:
            # Switching off AFTER ramping
            delay(self._RAMP_STEP_PERIOD)
            self._trap_rf.sw.off()

        # Guarantee events are submitted
        self.core.wait_until_mu(now_mu())

    @host_only
    def re_ramp(self, pause: float = 0.0) -> None:
        """Slowly ramp the trap RF amplitude down before ramping back up again to the last amplitude.

        :param pause: Pause time between ramp down and ramp up in seconds
        :raises KeyError: Raised if no previous value for frequency or amplitude is available
        """
        assert isinstance(pause, float) and pause >= 0.0, "Pause must be of type float and greater or equal to zero"

        # Store last amplitude
        amp = self.get_amp()
        # Ramp down to minimum amplitude
        self.ramp(self._MIN_AMP, enabled=False)

        if pause > 0.0:
            # Pause for the given duration
            self.logger.debug(f"Pausing for {pause} seconds")
            time.sleep(pause)

        # Ramp back up to the last amplitude and frequency
        self.ramp(amp)

    @host_only
    def off(self) -> None:
        """Switch off the trap RF by slowly ramping the amplitude to zero."""
        self.ramp(self._MIN_AMP, enabled=False)

    @host_only
    def safety_ramp_down(self):
        """Ramp down trap RF based on last cached amplitude, **should only be used in case of problems**.

        This function has limited checks and configuration to allow ramping down in a wide set of scenarios.
        """

        # Get last ASF from cache
        last_asf = self.core_cache.get(self._cache_last_asf_key)

        # Check if the system was just booted
        boot: bool = len(last_asf) == 0
        self.logger.debug(f"Safety ramp down trap RF, system boot detected: {boot}")

        if boot:
            # If the device was just booted, we cannot perform a safety ramp down
            self.logger.warning("cannot perform safety ramp down, no cached amplitude found")
            return

        try:
            # Default to the last stored frequency
            freq: float = self.get_last_freq()
            self.logger.info(f"Safety ramp down trap RF, using last frequency: {freq_to_str(freq)}")
        except KeyError:
            # Fall back on the resonance frequency
            freq = self._freq
            self.logger.info(f"Safety ramp down trap RF, fallback on resonance frequency: {freq_to_str(freq)}")
        assert isinstance(freq, float)

        # Load constants
        enabled: bool = False
        amp: float = self._MIN_AMP
        asf: int = self._trap_rf.amplitude_to_asf(amp)
        assert asf == 0

        # Decide the number of steps we need
        num_steps: int = (last_asf[0] - asf) + 1
        num_steps = max(num_steps, 2)  # We cannot have less than two steps
        assert num_steps >= 2, "Number of steps must be at least 2"

        # Create ASF list
        asf_list = np.linspace(last_asf[0], asf, num=num_steps, endpoint=True, dtype=np.int32)
        assert asf_list.min() == 0
        assert asf_list.max() < 2 ** 14
        assert asf_list.size <= 2 ** 14

        # Call the kernel function
        self.logger.debug(f"Safety ramp down trap RF, created ramp with {len(asf_list)} step(s)")
        self.logger.info(f"Safety ramping trap RF amplitude ({time_to_str(len(asf_list) * self._RAMP_STEP_PERIOD)})...")
        self._ramp(self._trap_rf.frequency_to_ftw(freq), asf_list, boot, enabled)
        self.logger.info("Trap RF amplitude safety ramping done")

        # Store latest values
        self.set_dataset_sys(self._ENABLED_KEY, enabled)
        self.set_dataset_sys(self._LAST_AMP_KEY, amp)
        self.set_dataset_sys(self._LAST_FREQ_KEY, freq)

    """Helper functions"""

    @host_only
    def get_amp(self, *, fallback: bool = False) -> float:
        """Get the last registered trap RF amplitude.

        The key for the last trap RF amplitude is private for safety reasons.
        This function can be used instead, even at build() time.

        Note: This value represents the last registered value and there is no guarantee that
        this is the actual current value at the device level.

        :param fallback: Allow fallback on the minimum value if no amplitude is available
        :return: Last trap RF amplitude
        :raises KeyError: Raised if no previous value was available
        """
        assert isinstance(fallback, bool), "Fallback flag must be a bool"
        amp: float  # Helps the type checker
        amp = self.get_dataset_sys(self._LAST_AMP_KEY, fallback=self._MIN_AMP if fallback else NoDefault)
        return amp

    @host_only
    def get_last_freq(self, *, fallback: bool = False) -> float:
        """Get the last registered trap RF frequency.

        The key for the last trap RF frequency is private for safety reasons.
        This function can be used instead, even at build() time.

        Note: This value represents the last registered value and there is no guarantee that
        this is the actual current value at the device level.

        :param fallback: Allow fallback on the resonance frequency if none is available
        :return: Last trap RF frequency
        :raises KeyError: Raised if no previous value was available
        """
        assert isinstance(fallback, bool), "Fallback flag must be a bool"
        freq: float  # Helps the type checker
        freq = self.get_dataset_sys(self._LAST_FREQ_KEY, fallback=self.get_freq() if fallback else NoDefault)
        return freq

    @host_only
    def is_enabled(self) -> bool:
        """Return the trap RF enabled flag.

        The key for the trap RF enabled flag is private for safety reasons.
        This function checks if the device was just rebooted and if the trap RF enabled flag was set.
        This function does not check if the trap RF amplitude meets some minimum value.

        Note: This function is safe to use at any time, even after a reboot.

        :return: True if trap RF is enabled
        :raises KeyError: Raised if no previous value was available, which means the state is ambiguous
        """
        if not self._system.dax_sim_enabled:
            # Check if the system was just booted
            last_asf = self.core_cache.get(self._cache_last_asf_key)
            if len(last_asf) == 0:
                # Device was just booted, trap RF is off
                return False

        # Return the enabled flag stored as a system dataset
        # Can raise a KeyError if the key was not set before, which means the state is ambiguous
        enabled: bool = self.get_dataset_sys(self._ENABLED_KEY)  # Helps the type checker
        return enabled

    @host_only
    def get_freq(self) -> float:
        """Get the frequency.

        Safe to call during :func:`build`.

        :return: The stored resonance frequency or the default
        """
        freq: float  # Helps the type checker
        freq = self.get_dataset_sys(self._FREQ_KEY, fallback=self._factory_freq)
        return freq

    @host_only
    def get_max_amp(self) -> float:
        """Get the maximum amplitude.

        Safe to call during :func:`build`.

        :return: The stored maximum amplitude or the default
        """
        amp: float = self.get_dataset_sys(self._MAX_AMP_KEY, fallback=self._MAX_AMP)  # Helps the type checker
        return amp

    @host_only
    def get_ramp_slope(self) -> float:
        """Get the ramp slope.

        Safe to call during :func:`build`.

        :return: The stored ramp slope or the default in asf/s
        """
        slope: float  # Helps the type checker
        slope = self.get_dataset_sys(self._RAMP_SLOPE_KEY, fallback=self._factory_ramp_slope)
        return slope

    @host_only
    def get_ramp_compression(self) -> bool:
        """Get the ramp compression flag.

        Safe to call during :func:`build`.

        :return: The stored ramp compression flag or the default
        """
        compression: bool  # Helps the type checker
        compression = self.get_dataset_sys(self._RAMP_COMPRESSION_KEY, fallback=self._FACTORY_RAMP_COMPRESSION)
        return compression

    @host_only
    def set_freq(self, freq: float) -> None:
        """set frequency.

        :param freq: Frequency in Hz
        """
        assert isinstance(freq, float)
        assert 0 < freq < 400 * MHz
        self._freq = freq
        self.set_dataset_sys(self._FREQ_KEY, freq)

    @host_only
    def set_max_amp(self, amp: float) -> None:
        """Set maximum amplitude.

        :param amp: Maximum trap RF amplitude for device protection
        """
        assert isinstance(amp, float)
        assert self._MIN_AMP <= amp <= self._MAX_AMP
        self._max_amp = amp
        self.set_dataset_sys(self._MAX_AMP_KEY, amp)

    @host_only
    def set_ramp_slope(self, slope: float) -> None:
        """Set ramp slope.

        :param slope: Ramp slope in asf/s
        """
        assert isinstance(slope, float)
        assert slope > 0
        self._ramp_slope = slope
        self.set_dataset_sys(self._RAMP_SLOPE_KEY, slope)

    @host_only
    def set_ramp_compression(self, compression: bool) -> None:
        """Set ramp compression.

        Ramp compression, enable for more efficient ramps by removing duplicate ramp values (can increase ramp speed).

        :param compression: Flag to enable or disable ramp compression
        """
        assert isinstance(compression, bool)
        self._ramp_compression = compression
        self.set_dataset_sys(self._RAMP_COMPRESSION_KEY, compression)
