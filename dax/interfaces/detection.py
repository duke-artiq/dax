import abc
import typing

import artiq.coredevice.edge_counter

from dax.base.interface import DaxInterface

__all__ = [
    'StateDetectionThresholdInterface',
    'DefaultDetectionTimeInterface',
    'PmtArrayInterface',
    'DetectionInterface',
]


class StateDetectionThresholdInterface(DaxInterface, abc.ABC):  # pragma: no cover
    """Interface to obtain the state detection threshold."""

    @abc.abstractmethod
    def get_state_detection_threshold(self) -> int:
        """Get the state detection threshold.

        :return: State detection threshold
        """
        pass


class DefaultDetectionTimeInterface(DaxInterface, abc.ABC):  # pragma: no cover
    """Interface to obtain the default detection time."""

    @abc.abstractmethod
    def get_default_detection_time(self) -> float:
        """Get the default detection time.

        :return: Detection time
        """
        pass


class PmtArrayInterface(DaxInterface, abc.ABC):  # pragma: no cover
    """Interface to obtain the PMT array (i.e. :class:`EdgeCounter` array)."""

    @abc.abstractmethod
    def get_pmt_array(self) -> typing.List[artiq.coredevice.edge_counter.EdgeCounter]:
        """Get the array of PMT channels.

        :return: A list with EdgeCounter objects
        """
        pass


class DetectionInterface(  # pragma: no cover
    StateDetectionThresholdInterface,
    DefaultDetectionTimeInterface,
    PmtArrayInterface,
    abc.ABC
):
    """Combination of the :class:`StateDetectionThresholdInterface`, :class:`DefaultDetectionTimeInterface`, and
    :class:`PmtArrayInterface` interfaces.

    This is the legacy detection interface with abstract methods from multiple fine-grained interfaces.
    For new implementations, we recommend implementing the fine-grained interfaces instead.
    """
    pass
