import importlib
import logging
import os
import shlex
from sipyco.pc_rpc import Client
import subprocess
import time
from typing import Any, Dict, TYPE_CHECKING


__all__ = ["LocalControllerAdapter", "SubprocessControllerAdapter"]


_logger: logging.Logger = logging.getLogger(__name__)
"""The logger for this file."""


class LocalControllerAdapter:
    """Run a controller as a local driver using this adapter.

    When using this adapter, the controller class is instantiated and called directly.
    Only controller classes that do not need a dedicated process are potentially suitable for this adapter.
    """

    _is_context: bool
    _controller: Any

    def __init__(
        self,
        dmgr: Any,
        controller_module: str,
        controller_class: str,
        arguments: Dict[str, Any],
        is_context: bool = True,
    ) -> None:
        """Initialize the local controller adapter.

        :param dmgr: Device manager
        :param controller_module: The module of the local controller as a string
        :param controller_class: The class of the local controller as a string
        :param arguments: Keyword arguments passed to the local controller class
        :param is_context: Flag to indicate the controller is a context and enter/exit should be called
        """
        assert isinstance(controller_module, str)
        assert isinstance(controller_class, str)
        assert isinstance(arguments, dict)
        assert all(isinstance(k, str) for k in arguments)
        assert isinstance(is_context, bool)

        # Store attributes
        self._is_context = is_context

        # Try to load controller class
        try:
            mod = importlib.import_module(controller_module)
            cls = getattr(mod, controller_class)
        except (ImportError, AttributeError) as e:
            raise ImportError(f'Cannot import controller class "{controller_module}.{controller_class}"') from e

        # Create controller class
        self._controller = self.create_controller(cls, **arguments)

        if self._is_context:
            # Call enter
            self._controller.__enter__()

    @staticmethod
    def create_controller(cls: Any, *args: Any, **kwargs: Any) -> Any:
        """Create the controller.

        This method can be overridden to allow more complex controller creation.
        """
        return cls(*args, **kwargs)

    def close(self) -> None:
        """Close this driver."""
        if self._is_context:
            # Call exit
            self._controller.__exit__(None, None, None)

    def __getattr__(self, item: str) -> Any:
        # Dynamically return attributes from controller
        return getattr(self._controller, item)


# Workaround required for Python<3.9
if TYPE_CHECKING:  # pragma: no cover
    _POPEN_T = subprocess.Popen[str]
else:
    _POPEN_T = subprocess.Popen


class SubprocessControllerAdapter:
    """Run a controller as a subprocess using this adapter.

    When using this adapter, a subprocess is spawned for the controller and an RPC client is instantiated.
    """

    _port: int
    _term_timeout: float
    _proc: _POPEN_T
    _client: Client

    def __init__(
        self,
        dmgr: Any,
        port: int,
        command: str,
        host: str = "::1",
        term_timeout: float = 6.2,
        connect_attempts: int = 10,
        connect_sleep: float = 0.1,
    ) -> None:
        """Initialize the subprocess controller adapter.

        :param dmgr: Device manager
        :param port: The port to run this controller on
        :param command: The command to start the controller
        :param host: The host to bind the controller to
        :param term_timeout: Termination timeout
        :param connect_attempts: Number of attempts to connect the client
        :param connect_sleep: Amount of time to sleep between client connect attempts (in seconds)
        """
        assert isinstance(port, int)
        assert isinstance(command, str)
        assert isinstance(host, str)
        assert isinstance(term_timeout, float)
        assert isinstance(connect_attempts, int)
        assert isinstance(connect_sleep, float)

        # Store attributes
        self._port = port
        self._term_timeout = term_timeout

        # Format command inplace
        command = command.format(bind=host, port=port)

        # Copy environment
        env = os.environ.copy()
        env["PYTHONUNBUFFERED"] = "1"

        # Create subprocess
        self._proc = subprocess.Popen(
            shlex.split(command),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            start_new_session=True,
            universal_newlines=True,
            env=env,
        )

        try:
            for _ in range(connect_attempts):
                code = self._proc.poll()
                if code is None:
                    try:
                        # Create client
                        self._client = Client(host, port)
                    except ConnectionError:
                        # Wait longer to allow subprocess to start
                        time.sleep(connect_sleep)
                    else:
                        break
                else:
                    err = "\n".join(self._proc.stdout.readlines()) if self._proc.stdout else ""
                    raise Exception(
                        f"subprocess terminated unexpectedly with return code {code}\ncommand={command}\n{err}"
                    )
            else:
                raise Exception(f"failed to create client for subprocess\ncommand={command}")

        except Exception:
            # Close subprocess before raising error again
            self.__close_subprocess()
            raise

    def __close_client(self) -> None:
        """Close client."""
        try:
            self._client.close_rpc()
        except Exception:
            _logger.exception(f"failed to close client for port {self._port}")

    def __close_subprocess(self) -> None:
        """Close subprocess."""
        if self._proc is None or self._proc.returncode is not None:
            _logger.info(f"subprocess for port {self._port} already terminated")
            return
        _logger.debug(f"terminating subprocess for port {self._port}")

        if os.name != "nt":
            try:
                self._proc.terminate()
            except ProcessLookupError:
                pass
            try:
                self._proc.wait(self._term_timeout)
                _logger.info(f"controller process for port {self._port} terminated")
                return
            except TimeoutError:
                _logger.warning(f"controller process for port {self._port} did not terminate, killing")
        try:
            self._proc.kill()
        except ProcessLookupError:
            pass
        try:
            self._proc.wait(self._term_timeout)
            _logger.info(f"controller process for port {self._port} killed")
            return
        except TimeoutError:
            _logger.warning(f"controller process for port {self._port} failed to die")

    def close(self) -> None:
        """Close this driver"""
        try:
            self.__close_client()
        finally:
            self.__close_subprocess()

    def __getattr__(self, item: str) -> Any:
        # Dynamically return attributes from client
        return getattr(self._client, item)
