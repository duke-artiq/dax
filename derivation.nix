{ callPackage
, buildPythonPackage
, nix-gitignore
, lib
, daxVersion ? null
, numpy
, scipy
, pyvcd
, natsort
, pygit2
, matplotlib
, graphviz
, h5py
, networkx
, sortedcontainers
, artiq
, sipyco
, trap-dac-utils ? callPackage (import (builtins.fetchGit { url = "https://gitlab.com/duke-artiq/trap-dac-utils.git"; })) { }
, pytestCheckHook
}:

buildPythonPackage rec {
  pname = "dax";
  version = if daxVersion == null then callPackage ./version.nix { inherit src; } else daxVersion;
  src = nix-gitignore.gitignoreSource [ "*.nix" "/*.lock" "/benches/" ] ./.;

  VERSIONEER_OVERRIDE = version;
  inherit (pygit2) SSL_CERT_FILE;

  dontWrapQtApps = true;

  propagatedBuildInputs = [
    numpy
    scipy
    pyvcd
    natsort
    pygit2
    matplotlib
    graphviz
    h5py
    networkx
    sortedcontainers
    artiq
    sipyco
    trap-dac-utils
  ];

  checkInputs = [ pytestCheckHook ];

  condaDependencies = [
    "python>=3.7"
    "numpy<2"
    "scipy"
    "pyvcd"
    "natsort"
    "pygit2"
    "matplotlib"
    "python-graphviz"
    "h5py"
    "networkx"
    "sortedcontainers"
    "artiq"
    "sipyco"
    "trap-dac-utils"
    "llvmlite<0.40.0" # Limit version, see https://gitlab.com/duke-artiq/dax/-/issues/144
  ];

  meta = with lib; {
    description = "Duke ARTIQ Extensions (DAX)";
    maintainers = [ "Duke University" ];
    homepage = "https://gitlab.com/duke-artiq/dax";
    license = licenses.mit;
  };
}
