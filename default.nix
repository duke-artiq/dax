{ pkgs ? import <nixpkgs> { }
, artiqpkgs ? import <artiq-full> { inherit pkgs; }
, daxVersion ? null
, trap-dac-utils ? pkgs.python3Packages.callPackage (import (builtins.fetchGit { url = "https://gitlab.com/duke-artiq/trap-dac-utils.git"; })) { }
}:

pkgs.python3Packages.callPackage ./derivation.nix { inherit daxVersion trap-dac-utils; inherit (artiqpkgs) artiq sipyco; }
